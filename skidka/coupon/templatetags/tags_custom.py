from django import template
import datetime

from ..models import Coupon, UsageCoupon

register = template.Library()

@register.simple_tag()
def get_date_value(value):
    return datetime.datetime.strptime(value, '%Y-%m-%d').strftime('%d.%m.%Y')


@register.simple_tag()
def get_coupon_code(value):
    asd = UsageCoupon.objects.filter(usage__id = value)
    [print(i.code) for i in asd]
    print('*'*150)
    return value