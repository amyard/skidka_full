# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2019-07-26 01:26
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('coupon', '0002_auto_20190603_0534'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usagecoupon',
            name='usage',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='usage_activate', to='coupon.Coupon', verbose_name='активация'),
        ),
    ]
