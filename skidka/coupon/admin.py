from django.contrib import admin
from .models import Coupon, CouponImage, UsageCoupon
from account.models import(
            Client, Discounter
        )
# Register your models here.


class UsageCouponInline(admin.StackedInline):
    readonly_fields=('code', 'client')
    model = UsageCoupon
    extra = 0 

class AlbumTaskInline(admin.StackedInline):
    
    model = CouponImage
    extra = 0
    
class AdminCoupon(admin.ModelAdmin):
    readonly_fields=(
        'discounter',
        'repeatedly',
        )
    inlines = (AlbumTaskInline, UsageCouponInline)

class AdminCouponImage(admin.ModelAdmin):
    pass

class CouponInlineAdmin(admin.StackedInline):
    model = Coupon
    extra = 0

class UsageCouponAdmin(admin.ModelAdmin):
    # inlines = (CouponInlineAdmin, )
    readonly_fields=('code', 'usage', 'client')

admin.site.register(Coupon, AdminCoupon)
admin.site.register(CouponImage, AdminCouponImage)
admin.site.register(UsageCoupon, UsageCouponAdmin)