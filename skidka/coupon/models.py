""" Дискоунтеры создают скидочные купоны.
Скидочный купон имеет время действия и количество раз использования.
Каждый пользователь может добавить купон в избранное и активировать купон.
"""
import datetime
from django.utils import timezone
from django.db import models
from django.utils.translation import ugettext_lazy as _
from account.models import RateActivate
from django.core.exceptions import ObjectDoesNotExist

from django.utils.text import slugify
from pytils import translit


def get_image_filename(instance, filename):
    # сохраняем фотки купона в общую директорию компании
    # media/<company_name>/файлы
    filename = str(instance.coupon.id) + '.jpg'
    cl_co = instance.coupon.discounter.company_name.replace(' ', '_')
    new_cl_co = translit.translify(slugify(cl_co, allow_unicode = True))
    return '{}/{}'.format(new_cl_co, filename)

def timer(month):
    t = datetime.datetime.now() + datetime.timedelta(month*365/12)
    #print(type(t.strftime("%Y-%m-%d %H:%M:%S")))
    #print(type(datetime.datetime.strptime(t.strftime("%Y-%m-%d %H:%M:%S"), "%Y-%m-%d %H:%M:%S")))
    return t.strftime("%Y-%m-%d %H:%M:%S")#datetime.datetime.strptime(str(t), "%Y-%m-%d %H:%M:%S.%f")




class Coupon(models.Model):
    """ скидочный купон
    имеет описание, срок действия исхдя из разности даты регистрации
    и даты окончания, заявленное и фактическое кол-во раз использования.
    """

    # TIME_CHOICES = (
    #     (0, _(u"не выбрано")),
    #     (timer(1), _(u"1 месяц")),
    #     (timer(2), _(u"2 месяца")),
    #     (timer(3), _(u"3 месяца")),
    #     (timer(4), _(u"4 месяца")),
    #     (timer(5), _(u"5 месяцев")),
    #     (timer(6), _(u"6 месяцев"))
    # )
    
    
    discounter = models.ForeignKey(
        'account.Discounter',
        related_name='coupon_discounter',
        on_delete=models.CASCADE
        )
    
    description = models.CharField(max_length=1500, verbose_name='описание')
    percent = models.CharField(max_length=3, verbose_name='размер скидки')
    #дата окончания срока действия купона
    validity = models.DateField(
        default=None,
        null=True,
        blank=True,
        verbose_name='действителен до',  
        max_length=30
        ) 
    
    # число купонов
    limited_quantity = models.PositiveIntegerField(
            default=None, 
            null=True,
            blank=True,
            verbose_name='ограничить кол-во купонов до...')
    
    # остаток купонов
    coupon_balance = models.PositiveIntegerField(
        default=None,
        null=True,
        blank=True,
        verbose_name='осталось купонов')
    
    # дата создания купона
    init_time = models.DateField(
        default=timezone.now,
        null=True,
        blank=True,
        verbose_name='создано'
        )
    
    activate = models.BooleanField(default=False, verbose_name='доступен')
    # заявленное количество раз использование
    repeatedly = models.PositiveIntegerField(default=1, verbose_name='кол-во раз на использование')
    

    def __str__(self):
        return '{}, {}'.format(self.discounter.company_name, self.description)
    
    def save(self, *args, **kwargs):
        # чекаем актуальность тарифа пользователя
        # и если тариф актуалаен ( больше нынешней даты), 
        # возвращаем сохраняем купоны с activate = True
        try:
            rate = RateActivate.objects.get(discounter=self.discounter, status=True)
            if rate.validity > datetime.datetime.now(timezone.utc):
                self.activate = True
                return super(Coupon, self).save(*args, **kwargs)
            else:
                # иначе выводим рэйз о сроке действия
                # на всякий пожарный 
                # это сообщение будет поймано при запросе в
                # api на создание купона.
                raise Exception('срок дейстсия тарифа закончен')
        
        
        except RateActivate.DoesNotExist:
            # в этом случае тарифа нет вообще
            raise Exception('для добавления купонов необходимо выбрать тариф')

    class Meta:
        verbose_name = "купон"
        verbose_name_plural = "купоны"


class CouponImage(models.Model):
    """ альбом купона. Все изображения купона собираются 
    в альбом. Для создания альбома каждая фотографие выделена
    в отедльный объект. 
    
    !Сохраняем все фоты в альбом (директорию) компании
    
    """
    coupon = models.ForeignKey(
        Coupon,
        default=None,
        related_name='album',
        verbose_name='купон')
    image = models.ImageField(upload_to=get_image_filename,
                              verbose_name='Image')
    
    class Meta:
        verbose_name = "изображение"
        verbose_name_plural = "изображения"


class UsageCoupon(models.Model):
    """ Активация 
    """
        # использовалось
    usage = models.ForeignKey('Coupon',
            default=None,
            verbose_name='активация', 
            related_name="usage_activate",
            null=True,
            blank=True,
            on_delete=models.CASCADE,
            )
    client = models.ForeignKey('account.Client',
            default=None,
            verbose_name='клиент', 
            related_name="usage_client",
            null=True,
            blank=True,
            )
    
    usage_date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    code = models.PositiveIntegerField(null=True, blank=True)
    
    def __str__(self):
        return "активация от {}, {}, купон: {}".format(
                    self.usage_date,
                    self.client,
                    self.usage,
                    )
    class Meta:
        verbose_name = "активация купона"
        verbose_name_plural = "активация купонов"