from django.shortcuts import render_to_response
from django.template import RequestContext


def page_not_found_view(request, *args, **kwargs):
    context = RequestContext(request)
    context['msg'] = 'страница не найдена'
    return render_to_response('404.html', context.flatten())


def handler500(request, *args, **argv):
    context = RequestContext(request)
    context['msg'] = 'что-то пошло не так'
    return render_to_response('500.html', context.flatten())