"""discount URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.views.static import serve
from django.conf.urls.static import static
from .err_response import page_not_found_view

from account.views import ResetSuccessView, ResetPasswordView
from api.views import (BasePageApiView, SetPasswordCustomApi, LoginCustomApi, SetPasswordSuccessApi, RegistraionApi,
                        RegistraionCreateApi, FaqCustomApi, ContactsCustomApi, RatesCustomApi, CouponCreateCustomApi,
                       CouponMyCustomApi, LoginCustomViews, GetNewPassView, )

from django.contrib.auth.views import logout

admin.site.site_header = settings.ADMIN_SITE_HEADER
#admin.site.site_title = settings.ADMIN_SITE_TITLE  
admin.site.index_title = settings.ADMIN_INDEX_TITLE

handler404 = page_not_found_view
#handler500 = handler500


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    
    # custom urls
    url(r'^users/set_password/$', SetPasswordCustomApi.as_view(), name='set_password'),
    url(r'^users/get_password/$', GetNewPassView.as_view(), name='get_password_cs'),

    url(r'^users/set_password/success/$', SetPasswordSuccessApi.as_view(), name='set_password_success'),
    url(r'^users/login/$', LoginCustomApi.as_view(), name='login_cs'),
    url(r'^login_user/$', LoginCustomViews.as_view(), name='login_user'),
    url(r'^logout/$', logout, {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout_cs'),

    url(r'^users/registration/$', RegistraionApi.as_view(), name='reg_cs'),
    url(r'^users/recall_discount/$', RegistraionCreateApi.as_view(), name='recall_discounter'),

    url(r'^faq/$', FaqCustomApi.as_view(), name="faq_cs"),
    url(r'^contacts/$', ContactsCustomApi.as_view(), name="contacts_cs"),
    url(r'^rates/$', RatesCustomApi.as_view(), name="rates_cs"),

    url(r'^coupon/create/$', CouponCreateCustomApi.as_view(), name="coupon_create_cs"),
    url(r'^coupon/my/$', CouponMyCustomApi.as_view(), name="coupon_my_cs"),



    url(r'^api/v1/', include('api.urls')),
    url(r'^users/', include('account.urls')),
    url(r'^media/(?P<path>.*)$', serve,{'document_root': settings.MEDIA_ROOT}), 
    url(r'^static/(?P<path>.*)$', serve,{'document_root': settings.STATIC_ROOT}),
    

    url(r'^$', BasePageApiView.as_view(), name='base'),
]

if settings.DEBUG == False:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)