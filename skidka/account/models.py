"""
СуперАдминистатор в единственном числе имеет все права,
в том числе право назначим дугим моделям все права. 
Разные модели пользователей одно право авторизации в бэкенде django. 
Все пользователи наследуются один к одному от метакласса.

dynamic_username – аттрибует класса метакласса пользователей, 
    динамический логин, принимает на запись значение почты,
    если поьзователь дискоунтер и админ или телефона, если 
    пользователь клиент. При создании моделей принимает значения
    полей email или телефон.

методы модуля:
    generate_filename – генерация директории и имени файла (.jpg)
    generate_user_filename – генерация директории и имени фала для юзеров
    timer – взвращает дату окончания действия купона
    pre_save_email_activation – обрабтка до отправки письма и создания модели активации
    post_save_user_create_reciever – создает инстанс объекта вызывает метод отправки сообщения

классы модуля:
    UserManager() – объектный менеджер класса SkdkUser
    SkdkUser - метакласс пользователей(
                    клиенты(Client),
                    дискоунтеры(Discounter),
                    субадминистраторы(AdminUser)
                    )
    UserClientManager объектный менеджер класса Client
    Client – клиент
    Discounter – дискоунтер
    DiscountBranch – филиал магазина(дискоунтера)
    Rate – тариф организации(магазина)
    EmailActivation - объект активации аккаунта(
                    !!!пока используется только для дискоунтеров
                    )
    EmailActivationManager – объектный менеджер класса EmailActivation
    EmailActivationQuerySet – кверисет записей

"""
import abc
import datetime
from datetime import timedelta, datetime
from django.conf import settings
from django.utils import timezone
from django.db import models
from django.contrib.auth.models import(
        AbstractBaseUser, BaseUserManager, PermissionsMixin
        )
from django.db.models import Q
from django.db.models.signals import pre_save, post_save
from django.core.mail import send_mail
from django.template.loader import get_template
from django.utils.translation import ugettext_lazy as _
from api.utils import random_string_generator, unique_key_generator
from geopy.geocoders import Yandex

DEFAULT_ACTIVATION_DAYS = getattr(settings, 'DEFAULT_ACTIVATION_DAYS', 7)

def geoloc(adress):
    geolocator = Yandex()
    adress = ' '.join(adress)
    location = geolocator.geocode(adress,  exactly_one=False)
    location = geolocator.reverse(
            list([location[0].latitude, location[0].longitude]),
            exactly_one=True
            )
    
    return location.raw['description'].split(',')[0]

def category_image_directory(instance, filename):
    # предположим, что у категории будут иконки
    filename = instance.title + '.jpg'
    return '{}'.format(instance.title) + "/" + '{}'.format(filename)

def generate_filename(instance, coupon_id, filename):
    # директория для файлов компании
    # media/еmail/<coupon_id>/файлы(.jpg)
    filename = random_string_generator(size=6) + '.jpg'
    return '{}'.format(instance.email) + "/"+ str(coupon_id)+ "/" + '{}'.format(filename)
    
def generate_user_filename(instance, filename):
    # директория для файлов юзеров
    # media/<phone>/файлы
    filename = instance.dynamic_username + '.jpg'
    return '{}'.format(instance.dynamic_username) + "/" + '{}'.format(filename)
    
def timer(month):
    # прибавляем montn к текущей дате
    t = datetime.now() + timedelta(month*365/12)
    return t.strftime("%Y-%m-%d 00:00:00")
    

class UserManager(BaseUserManager):
    """ класс менеджер созданя пользователей """   
    
    def _create_user(self, dynamic_username, password, email=None, phone=None, **extra_fields):
        """ создание обычного пользователя """
        if not dynamic_username:
            raise ValueError('Вы администратор, необходимо ввести email')
        
        if not password:
            raise ValueError('Необходимо заполнить пароль')
        
        email = self.normalize_email(dynamic_username)
        user = self.model(email=dynamic_username, phone=phone, dynamic_username=dynamic_username,  **extra_fields)
        user.set_password(password)
        #user.is_active = is_active
        user.save()
        return user
    
    def create_superuser(self, dynamic_username, password, **extra_fields):
        """ создание суперпользователя """
        
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)
        
        if extra_fields.get('is_staff') is not True:
            raise ValueError('Суперпользователь должен быть сотрудником')
        
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        return self._create_user(dynamic_username, password, **extra_fields)
        
    
class SkdkUser(AbstractBaseUser, PermissionsMixin):
    """
    Базовая модель пользователя. Имеет обищие поля данных для всех дочерних
    элементов. 
    Наследуется от абстрактной модели базового пользователя.
    Имеет:
        email,
        телефон,
        имя,
        фамилию,
        время регистрации,
        время обновления данных,
        статус персонала,
        статус активности профиля.
        
    """
    email = models.EmailField(unique=True, null=True, blank=True, verbose_name='email')
    phone = models.CharField(unique=True, max_length=17, null=True, blank=True, verbose_name='телефон')
    first_name = models.CharField(max_length=60, blank=True, verbose_name='имя')
    last_name = models.CharField(max_length=60, blank=True, verbose_name='фамилия')
    dynamic_username = models.CharField(max_length=60, unique=True, blank=True, null=True, verbose_name='login') 
    created_at   = models.DateTimeField(default=timezone.now, verbose_name='дата регистрации')
    updated_at   = models.DateTimeField(default=timezone.now, verbose_name='дата обновления')
    
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('обозначает, что пользователь может войти  панель администратора'),
    )
    
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Активированный аккаунт'
            'Деактивация акаунта не даст пользователю войти в систему'
        ),
    )
    
    #is_superuser = models.BooleanField(default=False, help_text='Обозначает, что этот пользователь имеет все разрешения без явного их назначения.', verbose_name='суперпользователь')
    USERNAME_FIELD = 'dynamic_username'
    
    objects = UserManager()
    
    def __str__(self):
        return "{}, {}".format(self.dynamic_username, self.phone)
    
    def get_dynamic_user_login(self):
        return self.dynamic_username
    
    def get_full_name(self):
        return self.dynamic_username

    def get_short_name(self):
        return self.first_name
    
  
    class Meta:
        verbose_name = "аккаунт"
        verbose_name_plural = "аккаунты"

class UserClientManager(BaseUserManager):
    
    def create_client(self, phone, password, **extra_fields):
        if not phone:
            raise ValueError('необходимо ввести телефон')
        
        if not password:
            raise ValueError('необходимо ввести пароль')
        
        user = self.model(phone=phone, dynamic_username=phone, **extra_fields)
        user.set_password(password)
        user.save()

        return user



class Client(SkdkUser):
    """ Аккаунт клиента - покупателя.
    Наследуется от базового класса пользователя User.
    Имеет доступ к своему акаунту в приложении, имя, аватар,
    пол и возраст, добавляет в избранное и покупает купоны.
    
    """
    GENDER_CHOICES = (
        (0, _(u"не выбрано")),
        (1, _(u"мужчина")),
        (2, _(u"женщина"))
    )

    
    avatar = models.ImageField(upload_to=generate_user_filename, blank=True, null=True, verbose_name='аватар')
    gender = models.SmallIntegerField(choices=GENDER_CHOICES, null=False, blank=True, default=0, verbose_name='пол')
    born = models.DateTimeField(default=datetime.now(), verbose_name='день рождения')
    age = models.IntegerField(null=True, blank=True,  verbose_name='возраст')
    # ключ доступа смены пароля, поумполчанию пуст
    # генерируется и записывается перед отправкой
    # письма. Удаляется после успешной смены парля
    reset_key = models.CharField(max_length=120, blank=True, null=True) 
    
    favorites = models.ManyToManyField(
        'coupon.Coupon',
        blank=True,
        verbose_name='избранное',
        related_name='favorites'
        )
    
    objects = UserClientManager()
    

    def get_favorites(self, _id):
        return self.favorites.__class__.objects.get(id=_id)
    
    def __str__(self):
        return '{},'.format(self.phone)

    class Meta:
        verbose_name = "клиент"
        verbose_name_plural = "клиенты"    


class RecallClient(models.Model):
    company = models.CharField(max_length=100, blank=True, verbose_name='компания')
    phone = models.CharField(unique=True, max_length=17, null=True, blank=True, verbose_name='телефон')
    full_name = models.CharField(max_length=60, blank=True, verbose_name='фио')

    class Meta:
        verbose_name = "перезвонить клиенту"
        verbose_name_plural = "перезвонить клиенту"   


class AdminUserManager(BaseUserManager):
    
    def create_subadmin(self, dynamic_username,  password, **extra_fields):
        """ создание администратора пользователя """
        if not dynamic_username:
            raise ValueError('Вы администратор, необходимо ввести email')
        
        if not password:
            raise ValueError('Необходимо заполнить пароль')
        
        email = self.normalize_email(dynamic_username)
        user = self.model(email=dynamic_username, dynamic_username=dynamic_username,  **extra_fields)
        user.set_password(password)
        user.save()
        return user

class AdminUser(SkdkUser):
    """ Аккаунт администраторов
    Наследуется от главного """
    
    objects = AdminUserManager()
    
    class Meta:
        verbose_name = "администратор"
        verbose_name_plural = "администраторы"



class DiscounterClientManager(BaseUserManager):
    """ Base User Manager """
    
    def create_discounter(self, email, password, is_active=False, **extra_fields):
        """ """
        if not email:
            raise ValueError('Необходимо ввести email')
        
        if not password:
            raise ValueError('Необходимо ввести пароль')
        
        email = self.normalize_email(email)
        user = self.model(email=email, dynamic_username=email, **extra_fields)
        user.set_password(password)
        user.is_active = is_active
        user.save()
        return user


class DiscounterCategory(models.Model):
    """ категория дискоунтера 
    Имеет: название, img
    """
    title = models.CharField(max_length=200, verbose_name='название категории')
    
    image = models.ImageField(
        upload_to=category_image_directory,
        blank=True,
        null=True
    )
    
    def __str__(self):
        return '{}'.format(self.title)
        
    class Meta:
        verbose_name = "категория организации"
        verbose_name_plural = "категории организации"

class DiscounterCity(models.Model):
    """ город """
    name = models.CharField(max_length=200, verbose_name='название')

    def __str__(self):
        return '{}'.format(self.name)
        
    class Meta:
        verbose_name = "город организации"
        verbose_name_plural = "город организации"

class Discounter(SkdkUser):
    """ Аккаунт компании дискоунтера - магазина.
    Наледуется от базового класса пользователя User.
    Имеет доступ к своему акаунту на сайте, список купонов,
    название, описание своей организации, адрес и контакты
    Создает свои купоны (избранное), использованные купоны
    и тариф.
    
    """
    # user = models.OneToOneField(settings.AUTH_USER_MODEL,
    #       on_delete=models.CASCADE,
    #       primary_key=True,
    # )
    discounter_сity = models.ForeignKey(
            DiscounterCity,
            on_delete=models.PROTECT,
            default=None,
            null=True,
            blank=True,
            verbose_name='город',
            related_name="discounter_сity"
            )
    category = models.ForeignKey(
            DiscounterCategory,
            default=None,
            null=True,
            blank=True,
            verbose_name='категория',
            related_name="category"
            )
    country = models.CharField(max_length=200, verbose_name='страна')
    city = models.CharField(max_length=200, verbose_name='город')
    street = models.CharField(max_length=200, verbose_name='улица')
    hnum = models.CharField(max_length=200, verbose_name='номер дома')
    avatar = models.ImageField(upload_to=generate_user_filename, blank=True, null=True, verbose_name='аватар')
    cover_image = models.ImageField(upload_to=generate_user_filename, blank=True, null=True, verbose_name='обложка')
    company_name = models.CharField(max_length=200, blank=True, null=True, verbose_name='название организации')
    address = models.CharField(max_length=200, null=True, blank=True,  verbose_name='адрес')
    description = models.CharField(max_length=500, null=True, blank=True,  verbose_name='описание')
    reset_key = models.CharField(max_length=120, blank=True, null=True)
    opening = models.TimeField(default=timezone.now, blank=True, null=True, verbose_name='время открытия')
    close = models.TimeField(default=timezone.now, blank=True, null=True, verbose_name='время закрытия')
    #pay_status = models.BooleanField(required=False, verbose_name='статус оплаты тарифа') # оплачено/неоплачено
    
    objects =  DiscounterClientManager()
    
    def __str__(self):
        return '{}, {}, {}'.format(self.email, self.first_name, self.company_name)
    
    class Meta:
        verbose_name = "дискоунтер"
        verbose_name_plural = "дискоунтеры"

 
class RateActivateManager(models.Manager):

    def get_validity(self):
        return "{}".format(self.model.validity)


class RateActivate(models.Model):
    """ Активация тарифа
    Выбираем основную модель тарифа,
    Инициализируем при создании даты акутивации,
    и вычитаем из времени(периода) тарифа.
    """
    discounter = models.ForeignKey(
        'Discounter',
        blank=True,
        null=True,
        verbose_name='активированный тариф',
        related_name='activate_rate'
        )
    
    init_time = models.DateTimeField(
        default=timezone.now,
        null=True,
        blank=True,
        verbose_name='дата создания'
        ) #дата инициализации тарифа

    rate = models.ForeignKey(
        'Rate',
        on_delete=models.CASCADE,
        default=None,
        null=True,
        blank=True,
        verbose_name='тариф',
        related_name='rate'
    )
    
    validity = models.DateTimeField(
        default=None,
        null=True,
        blank=True,
        verbose_name='дата окончания',
    ) #дата окончания
    
    status = models.BooleanField(default=True)
    #objects = RateActivateManager()
    

    def __str__(self):
        return '{}, {}'.format(self.init_time, self.validity) 
    

    class Meta:
        verbose_name = "активированный тариф"
        verbose_name_plural = "активированный тариф"


def post_save_rate_activate(sender, instance, created, *args, **kwargs):
    # ловим постсэйв тарифа
    # и инициализируем срок действия тарифа
    if created:
        obj = RateActivate.objects.last()
        obj.validity = timer(instance.rate.validity)
        obj.save()

post_save.connect(post_save_rate_activate, sender=RateActivate)


class DiscountBranch(models.Model):
    """ Фелилалы магазина 
    имеют название, описание, адрес, фото
    """
    avatar = models.ImageField(blank=True, null=True, verbose_name='аватар')
    parent_shop = models.ForeignKey(Discounter, null=True, blank=True, verbose_name='родитель', related_name="branch")
    name = models.CharField(max_length=200, blank=True, null=True, verbose_name='название')
    description = models.CharField(max_length=500, blank=True, null=True, verbose_name='описание')
    address = models.CharField(max_length=300,  blank=True, null=True, verbose_name='адррес')
    
    def __str__(self):
        return '{}, {}'.format(self.name, self.address)
    
    class Meta:
        verbose_name = "филиал"
        verbose_name_plural = "филиалы"


class Rate(models.Model):
    """ тариф организации (дискаунтера)
    имеет срок действия от 1 до 6 месяцев, статус оплаты, и описание.
    """
    TIME_CHOICES = (
        (0, _(u"не выбрано")),
        (1, _(u"1 месяц")),
        (2, _(u"2 месяца")),
        (3, _(u"3 месяца")),
        (4, _(u"4 месяца")),
        (5, _(u"5 месяцев")),
        (6, _(u"6 месяцев"))
    )
    
    validity = models.IntegerField(
        choices=TIME_CHOICES, 
        null=True,
        verbose_name='месяцы действия',
    ) #дата окончания
    
    name = models.CharField(max_length=200, verbose_name='название')
    description = models.TextField(max_length=1000, verbose_name='описание')

    def __str__(self):
        return ''.join(self.name)
    
    class Meta:
        verbose_name = "тариф"
        verbose_name_plural = "тарифы"
        
class EmailActivationQuerySet(models.query.QuerySet):
    
    def confirmable(self):
        now = timezone.now()
        start_range = now - timedelta(days=DEFAULT_ACTIVATION_DAYS)
        # does my object have a timestamp in here
        end_range = now
        return self.filter(
                activated = False,
                forced_expired = False
              ).filter(
                timestamp__gt=start_range,
                timestamp__lte=end_range
              )


class EmailActivationManager(models.Manager):
    
    def get_queryset(self):
        return EmailActivationQuerySet(self.model, using=self._db)

    def confirmable(self):
        return self.get_queryset().confirmable()
        
    def email_exists(self, email):
        return self.get_queryset().filter(
                    Q(email=email) | 
                    Q(user__email=email)
                ).filter(
                    activated=False
                )
                
class EmailActivation(models.Model):
    """  Активация email аккауента дискоунтера """
    
    user            = models.ForeignKey(Discounter, related_name='activate', on_delete=models.CASCADE)
    email           = models.EmailField()
    key             = models.CharField(max_length=120, blank=True, null=True)
    activated       = models.BooleanField(default=False)
    forced_expired  = models.BooleanField(default=False)
    expires         = models.IntegerField(default=7) # 7 Days
    timestamp       = models.DateTimeField(auto_now_add=True)
    update          = models.DateTimeField(auto_now=True)

    objects = EmailActivationManager()

    def __str__(self):
        return self.email

    def can_activate(self):
        qs = EmailActivation.objects.filter(pk=self.pk).confirmable() # 1 object
        if qs.exists():
            return True
        return False
        
    def activate(self):
        if self.can_activate():
            # предварительно  активируем джанго сигнал
            user = self.user
            user.is_active = True
            user.save()
            # последующая активация
            self.activated = True
            self.save()
            return True
        return False

    def regenerate(self):
        self.key = None
        self.save()
        if self.key is not None:
            return True
        return False

    def send_activation(self):
        if not self.activated and not self.forced_expired:
            if self.key:
                base_url = getattr(settings, 'BASE_URL', 'https://skidka71.yorichdevelop.ru/')
                #key_path = reverse("account:email-activate", kwargs={'key': self.key}) # use reverse
                key_path = 'api/v1/account/email/confirm/' + self.key +'/'
                path = "{base}{path}".format(base=base_url, path=key_path)
                context = {
                    'path': path,
                    'email': self.email
                }
                txt_ = get_template("registration/email/verify.txt").render(context)
                html_ = get_template("registration/email/verify.html").render(context)
                subject = 'skidka71  подтверждение аккаунта '
                from_email = settings.DEFAULT_FROM_EMAIL
                recipient_list = [self.email]
                sent_mail = send_mail(
                            subject,
                            txt_,
                            from_email,
                            recipient_list,
                            html_message=html_,
                            fail_silently=False,
                    )
                return sent_mail
        return False



def pre_save_email_activation(sender, instance, *args, **kwargs):
    # если перед записью объект аккант не активирован,
    # был отключен и ключ активации отсутсвует – записываем
    # ключ в инстанс объекта
    if not instance.activated and not instance.forced_expired:
        if not instance.key:
            instance.key = unique_key_generator(instance)

pre_save.connect(pre_save_email_activation, sender=EmailActivation)


def post_save_user_create_reciever(sender, instance, created, *args, **kwargs):
    # проверяем валидность записи
    # создаем экземпляр класса активации и
    # и записываем в него инстанс в качестве юзера
    # и email в качестве его почты
    if created:
        obj = EmailActivation.objects.create(user=instance, email=instance.email)
        obj.send_activation()

post_save.connect(post_save_user_create_reciever, sender=Discounter)
