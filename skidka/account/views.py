from django.contrib.auth import authenticate, login, get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import CreateView, FormView, DetailView, View, UpdateView, TemplateView
from django.views.generic.edit import FormMixin
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.utils.http import is_safe_url
from django.utils.safestring import mark_safe
from django.shortcuts import render_to_response
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from .forms import  ReactivateEmailForm
from .models import  EmailActivation, Client, Discounter




class AccountEmailActivateView(FormMixin, View):
    """
    Подтверждение почты. Если польз
    success_url = '/login/' 
    """
    success_url = '/login/'
    form_class = ReactivateEmailForm
    key = None
    template = 'registration/activate.html'
    
    def get(self, request, key=None, *args, **kwargs):
        self.key = key
        
        # если объект активации почты есть
        # берем объект активации по соответствию ключа активации
        
        if key is not None:
            
            qs = EmailActivation.objects.filter(key__iexact=key)
            confirm_qs = qs.confirmable()
            if confirm_qs.count() == 1:
                obj = confirm_qs.first()
                obj.activate()
                # заменить на return render(data)
                # messages.success(request, "Ваш email успешно подтвержден, пожалуйста, авторизируйтесь")
                return render(request, self.template)
            else:
                activated_qs = qs.filter(activated=True)
                if activated_qs.exists():
                    reset_link = reverse("password_reset")
                    msg = """Your email has already been confirmed
                    Do you need to <a href="{link}">reset your password</a>?
                    """.format(link=reset_link)
                    messages.success(request, mark_safe(msg))
                    return redirect("login") 
        
        context = {'form': self.get_form(),'key': key}
        return render(request, 'registration/activation-error.html', context)
    
    
    """
    def post(self, request, *args, **kwargs):
        # create form to receive an email
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        msg = "Activation link sent, please check your email."
        request = self.request
        messages.success(request, msg)
        email = form.cleaned_data.get("email")
        obj = EmailActivation.objects.email_exists(email).first()
        user = obj.user 
        new_activation = EmailActivation.objects.create(user=user, email=email)
        new_activation.send_activation()
        return super(AccountEmailActivateView, self).form_valid(form)

    def form_invalid(self, form):
        context = {'form': form, "key": self.key }
        return render(self.request, 'registration/activation-error.html', context)"""



class ResetPasswordView(FormMixin, View):
    """
    Подтверждение почты. Если польз
    success_url = '/login/' 
    """
    template_name = 'reset_pwsd_page.html'
    
    def get(self, request, reset_key=None, *args, **kwargs):
        
        try:
            if reset_key:
                try:
                    user = Client.objects.get(reset_key=reset_key)
                except:
                    user = Discounter.objects.get(reset_key=reset_key)
                
                return render(request, self.template_name, {
                                'username': user.first_name,
                                'msg': 'введите новый девятизначный пароль'
                            })
        except:
            return render(request, 'user_has_no_key.html', {
                                 'msg': 'Для смены авторизацинных данных необходимо отправить запрос на смену данных'
                            })
            
    
    def reset_pswd(self, password, user):
        try:
            user.set_password(password)
            user.reset_key = ''
            user.save()
            self.send_email_success(user.email, user.first_name, user.dynamic_username, password)
        
        except:
            pass
            
        return redirect('reset_success')
    
    def post(self, request, reset_key=None, *args, **kwargs):
        
        password = request.POST['pass']
        
        try:
            user = Client.objects.get(reset_key=reset_key)
        except:
            user = Discounter.objects.get(reset_key=reset_key)
        
        if len(password) < 9:
            return render(request, self.template_name, {
                                    'username': user.first_name,
                                    'msg': 'пароль не сохранен, введите новый девятизначный пароль.',              
                                })
            
        return self.reset_pswd(password, user)
        

    
    def send_email_success(self, email, full_name, login, password):
        # получаем мыло и имя пользователя
        # рендерим в шаблоны имя в контексте
        # отправляем письмо

        text = """Недавно вы изменили данные своей учетной записи на нашем сайте.
                ваш логин:""" + login + " ,  пароль: "+password+'.'
        
        plaintext = get_template('success_password.txt')
        htmly     = get_template('success_password.html')
        
        subject, from_email, to = 'Скидка71 изменение пароля', 'no-reply@skdk.com', email
        
        text_content = plaintext.render({ 'username': full_name })
        
        html_content = htmly.render({
            'username': full_name,
            'text': text
        })
        
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, "text/html")
        msg.send()
        return None

class ResetSuccessView(View):
    template_name = 'reset_success.html'
    
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, context={})
        