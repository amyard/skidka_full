from django.conf.urls import url

from .views import (
        AccountEmailActivateView,
        ResetPasswordView,
        ResetSuccessView
    )

from api.views import (
        ClientSelfAccountView,
        DiscounterAccountView,
        ClientFavoritesView,
        CouponDiscounterView,
        AddDiscountBranchView,
        DiscounterActivationView
    )

urlpatterns = [
    url(r'email/confirm/(?P<key>[0-9A-Za-z]+)/$', 
            AccountEmailActivateView.as_view(), 
            name='email-activate'),
    
    url('email/resend-activation/', 
            AccountEmailActivateView.as_view(), 
            name='resend-activation'),
    
    url(r'reset_pswd/(?P<reset_key>[0-9A-Za-z]+)/$', 
            ResetPasswordView.as_view(), 
            name='reset_password'),
    
    url(r'reset_success',
            ResetSuccessView.as_view(),
            name='reset_success'),
    #path('home/', AccountView.as_view()),
    url('client/', ClientSelfAccountView.as_view()),
    url('favorites/', ClientFavoritesView.as_view()),
    url('discounter/', DiscounterAccountView.as_view()),
    url('coupon/', CouponDiscounterView.as_view()),
    url('branch/', AddDiscountBranchView.as_view()),
    url('activations/', DiscounterActivationView.as_view())
]