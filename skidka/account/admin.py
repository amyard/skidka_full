from django import forms
from django.contrib import admin
from django.contrib.auth import password_validation
from django.db.models import Sum, Min, Max, ManyToManyField
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.contrib.auth.forms import UserChangeForm as BaseUserChangeForm
from django.utils.translation import gettext, gettext_lazy as _
from .models import (
    AdminUser,
    Client,
    Discounter,
    SkdkUser,
    Rate,
    DiscountBranch,
    EmailActivation,
    DiscounterCategory,
    DiscounterCity,
    RateActivate,
    RecallClient
    )
from coupon.models import Coupon
from django.forms import CheckboxSelectMultiple
from django.contrib.auth.admin import GroupAdmin
from django.contrib.auth.models import Group



class UserEmperrorCreationForm(forms.ModelForm):
    """ Форма для главного суперпользователя """
    password1 = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput,
        help_text=password_validation.password_validators_help_text_html(),
    )
    
    password2 = forms.CharField(
        label=_("Password confirmation"),
        widget=forms.PasswordInput,
        strip=False,
        help_text=_("Enter the same password as before, for verification."),
    )


    class Meta:
        model = SkdkUser
        fields = ('email', 'is_staff', 'is_active', 'first_name', 'last_name', )

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Паролли не совпадают")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        user.email = self.cleaned_data.get("dynamic_username")
        if commit:
            user.save()
        return user    
        

class CreationForm(forms.ModelForm):
    """ Форма для администраторов """
    
    password1 = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput,
        help_text=password_validation.password_validators_help_text_html(),
    )
    
    password2 = forms.CharField(
        label=_("Password confirmation"),
        widget=forms.PasswordInput,
        strip=False,
        help_text=_("Enter the same password as before, for verification."),
    )


    class Meta:
        model = AdminUser
        fields = ('email', 'is_staff', 'is_active', 'first_name', 'last_name', )

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Паролли не совпадают")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        user.email = self.cleaned_data.get("dynamic_username")
        #user.__class__.objects.create_subadmin(self.cleaned_data.get("dynamic_username"), self.cleaned_data.get("password1"))
        if commit:
            user.save()
        return user


class UserChangeForm(BaseUserChangeForm):
    pass

class UserEmperorAdmin(BaseUserAdmin):
    
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserEmperrorCreationForm
    
    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    
    def group(self, user):
        groups = []
        for group in user.groups.all():
            groups.append(group.name)
        return ' '.join(groups)
    
    group.short_description = 'должность'
        
    list_display = ('dynamic_username', 'email', 'is_superuser', 'is_staff', 'group', 'is_active', 'first_name', 'last_name', 'last_login')
    # list_filter = ('email',)
    
    readonly_fields=('created_at', 'updated_at',)
    
    fieldsets = (
            (None                , {'fields': ('dynamic_username', 'password')}),
            (_('Personal info')  , {'fields': ('email', 'phone', 'first_name', 'last_name',)}),
            (_('Permissions')    , {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')}),
            (_('Important dates'), {'fields': ('last_login', 'created_at', 'updated_at')}),
    )
        
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
        
    add_fieldsets = (
            (None, {
                'classes': ('wide',),
                'fields': ('dynamic_username', 'password1', 'password2')}
            ),
    )
        
    search_fields = ('email','dynamic_username')
    ordering = ('email', 'dynamic_username')
    
    filter_horizontal = ('groups', 'user_permissions')



class UserAdmin(BaseUserAdmin):
    
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = CreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    
    def group(self, user):
        groups = []
        for group in user.groups.all():
            groups.append(group.name)
        return ' '.join(groups)
    
    group.short_description = 'должность'
    
    list_display = ('dynamic_username', 'email', 'is_superuser', 'is_staff', 'group', 'is_active', 'first_name', 'last_name', 'last_login')
    # list_filter = ('email',)

    readonly_fields=('created_at', 'updated_at',)
    
    fieldsets = (
        (None                , {'fields': ('dynamic_username', 'password')}),
        (_('Personal info')  , {'fields': ('email', 'phone', 'first_name', 'last_name',)}),
        (_('Permissions')    , {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'created_at', 'updated_at')}),
    )
    
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('dynamic_username', 'password1', 'password2')}
        ),
    )
    
    search_fields = ('email', 'dynamic_username')
    ordering = ('email', 'dynamic_username')
    
    filter_horizontal = ('groups', 'user_permissions')

# class ClientProfileInline(admin.StackedInline):
#     model = Client    




class ClientAccountAdmin(admin.ModelAdmin):

    exclude = ('is_staff', 'is_superuser', 'groups', 'user_permissions')
    
    filter_horizontal = ('favorites',)
    
    list_display = (
          'email',
          'dynamic_username',
          'phone',
          )


class DiscountBranchInline(admin.StackedInline):
    
    model = DiscountBranch
    extra = 0

class RateActivateInline(admin.StackedInline):

    model = RateActivate
    extra = 0
    readonly_fields=('validity',)

class CouponInline(admin.StackedInline):
    model = Coupon
    extra = 0
    

class DiscounterAdmin(admin.ModelAdmin):
    
    exclude = ('is_staff', 'is_superuser', 'groups', 'user_permissions')
    
    #filter_horizontal = ('category',)
    
    list_display = (
        'email',
        'dynamic_username',
        )

    inlines = (RateActivateInline, CouponInline, DiscountBranchInline)

class AdminDiscounterCategory(admin.ModelAdmin):
    pass

class AdminDiscounterCity(admin.ModelAdmin):
    pass

class RateAdmin(admin.ModelAdmin):
    inlines = (RateActivateInline,)

class DiscountBranchAdmin(admin.ModelAdmin):
    pass

class AdminEmailActivation(admin.ModelAdmin):
    pass


class RecallClientAdmin(admin.ModelAdmin):
    list_display = ['company', 'phone', 'full_name']


admin.site.register(RecallClient, RecallClientAdmin)

admin.site.register(AdminUser, UserAdmin)
admin.site.register(SkdkUser, UserEmperorAdmin)
admin.site.register(Client, ClientAccountAdmin)
admin.site.register(Discounter, DiscounterAdmin)
admin.site.register(Rate, RateAdmin)
admin.site.register(DiscountBranch, DiscountBranchAdmin)
admin.site.register(EmailActivation, AdminEmailActivation)
admin.site.register(DiscounterCategory, AdminDiscounterCategory)
admin.site.register(DiscounterCity, AdminDiscounterCity)