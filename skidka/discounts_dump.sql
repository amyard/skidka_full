--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.15
-- Dumped by pg_dump version 9.6.15

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: account_adminuser; Type: TABLE; Schema: public; Owner: discounts_user
--

CREATE TABLE public.account_adminuser (
    skdkuser_ptr_id integer NOT NULL
);


ALTER TABLE public.account_adminuser OWNER TO discounts_user;

--
-- Name: account_client; Type: TABLE; Schema: public; Owner: discounts_user
--

CREATE TABLE public.account_client (
    skdkuser_ptr_id integer NOT NULL,
    avatar character varying(100),
    gender smallint NOT NULL,
    age integer,
    reset_key character varying(120),
    born timestamp with time zone NOT NULL
);


ALTER TABLE public.account_client OWNER TO discounts_user;

--
-- Name: account_client_favorites; Type: TABLE; Schema: public; Owner: discounts_user
--

CREATE TABLE public.account_client_favorites (
    id integer NOT NULL,
    client_id integer NOT NULL,
    coupon_id integer NOT NULL
);


ALTER TABLE public.account_client_favorites OWNER TO discounts_user;

--
-- Name: account_client_favorites_id_seq; Type: SEQUENCE; Schema: public; Owner: discounts_user
--

CREATE SEQUENCE public.account_client_favorites_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_client_favorites_id_seq OWNER TO discounts_user;

--
-- Name: account_client_favorites_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: discounts_user
--

ALTER SEQUENCE public.account_client_favorites_id_seq OWNED BY public.account_client_favorites.id;


--
-- Name: account_discountbranch; Type: TABLE; Schema: public; Owner: discounts_user
--

CREATE TABLE public.account_discountbranch (
    id integer NOT NULL,
    avatar character varying(100),
    name character varying(200),
    description character varying(500),
    address character varying(300),
    parent_shop_id integer
);


ALTER TABLE public.account_discountbranch OWNER TO discounts_user;

--
-- Name: account_discountbranch_id_seq; Type: SEQUENCE; Schema: public; Owner: discounts_user
--

CREATE SEQUENCE public.account_discountbranch_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_discountbranch_id_seq OWNER TO discounts_user;

--
-- Name: account_discountbranch_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: discounts_user
--

ALTER SEQUENCE public.account_discountbranch_id_seq OWNED BY public.account_discountbranch.id;


--
-- Name: account_discounter; Type: TABLE; Schema: public; Owner: discounts_user
--

CREATE TABLE public.account_discounter (
    skdkuser_ptr_id integer NOT NULL,
    avatar character varying(100),
    company_name character varying(200),
    address character varying(200),
    description character varying(500),
    reset_key character varying(120),
    category_id integer,
    city character varying(200) NOT NULL,
    country character varying(200) NOT NULL,
    hnum character varying(200) NOT NULL,
    street character varying(200) NOT NULL,
    "discounter_сity_id" integer,
    close time without time zone,
    opening time without time zone,
    cover_image character varying(100)
);


ALTER TABLE public.account_discounter OWNER TO discounts_user;

--
-- Name: account_discountercategory; Type: TABLE; Schema: public; Owner: discounts_user
--

CREATE TABLE public.account_discountercategory (
    id integer NOT NULL,
    title character varying(200) NOT NULL,
    image character varying(100)
);


ALTER TABLE public.account_discountercategory OWNER TO discounts_user;

--
-- Name: account_discountercategory_id_seq; Type: SEQUENCE; Schema: public; Owner: discounts_user
--

CREATE SEQUENCE public.account_discountercategory_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_discountercategory_id_seq OWNER TO discounts_user;

--
-- Name: account_discountercategory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: discounts_user
--

ALTER SEQUENCE public.account_discountercategory_id_seq OWNED BY public.account_discountercategory.id;


--
-- Name: account_discountercity; Type: TABLE; Schema: public; Owner: discounts_user
--

CREATE TABLE public.account_discountercity (
    id integer NOT NULL,
    name character varying(200) NOT NULL
);


ALTER TABLE public.account_discountercity OWNER TO discounts_user;

--
-- Name: account_discountercity_id_seq; Type: SEQUENCE; Schema: public; Owner: discounts_user
--

CREATE SEQUENCE public.account_discountercity_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_discountercity_id_seq OWNER TO discounts_user;

--
-- Name: account_discountercity_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: discounts_user
--

ALTER SEQUENCE public.account_discountercity_id_seq OWNED BY public.account_discountercity.id;


--
-- Name: account_emailactivation; Type: TABLE; Schema: public; Owner: discounts_user
--

CREATE TABLE public.account_emailactivation (
    id integer NOT NULL,
    email character varying(254) NOT NULL,
    key character varying(120),
    activated boolean NOT NULL,
    forced_expired boolean NOT NULL,
    expires integer NOT NULL,
    "timestamp" timestamp with time zone NOT NULL,
    update timestamp with time zone NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.account_emailactivation OWNER TO discounts_user;

--
-- Name: account_emailactivation_id_seq; Type: SEQUENCE; Schema: public; Owner: discounts_user
--

CREATE SEQUENCE public.account_emailactivation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_emailactivation_id_seq OWNER TO discounts_user;

--
-- Name: account_emailactivation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: discounts_user
--

ALTER SEQUENCE public.account_emailactivation_id_seq OWNED BY public.account_emailactivation.id;


--
-- Name: account_rate; Type: TABLE; Schema: public; Owner: discounts_user
--

CREATE TABLE public.account_rate (
    id integer NOT NULL,
    validity integer,
    name character varying(200) NOT NULL,
    description text NOT NULL
);


ALTER TABLE public.account_rate OWNER TO discounts_user;

--
-- Name: account_rate_id_seq; Type: SEQUENCE; Schema: public; Owner: discounts_user
--

CREATE SEQUENCE public.account_rate_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_rate_id_seq OWNER TO discounts_user;

--
-- Name: account_rate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: discounts_user
--

ALTER SEQUENCE public.account_rate_id_seq OWNED BY public.account_rate.id;


--
-- Name: account_rateactivate; Type: TABLE; Schema: public; Owner: discounts_user
--

CREATE TABLE public.account_rateactivate (
    id integer NOT NULL,
    init_time timestamp with time zone,
    validity timestamp with time zone,
    status boolean NOT NULL,
    rate_id integer,
    discounter_id integer
);


ALTER TABLE public.account_rateactivate OWNER TO discounts_user;

--
-- Name: account_rateactivate_id_seq; Type: SEQUENCE; Schema: public; Owner: discounts_user
--

CREATE SEQUENCE public.account_rateactivate_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_rateactivate_id_seq OWNER TO discounts_user;

--
-- Name: account_rateactivate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: discounts_user
--

ALTER SEQUENCE public.account_rateactivate_id_seq OWNED BY public.account_rateactivate.id;


--
-- Name: account_skdkuser; Type: TABLE; Schema: public; Owner: discounts_user
--

CREATE TABLE public.account_skdkuser (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    email character varying(254),
    phone character varying(17),
    first_name character varying(60) NOT NULL,
    last_name character varying(60) NOT NULL,
    dynamic_username character varying(60),
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL
);


ALTER TABLE public.account_skdkuser OWNER TO discounts_user;

--
-- Name: account_skdkuser_groups; Type: TABLE; Schema: public; Owner: discounts_user
--

CREATE TABLE public.account_skdkuser_groups (
    id integer NOT NULL,
    skdkuser_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.account_skdkuser_groups OWNER TO discounts_user;

--
-- Name: account_skdkuser_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: discounts_user
--

CREATE SEQUENCE public.account_skdkuser_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_skdkuser_groups_id_seq OWNER TO discounts_user;

--
-- Name: account_skdkuser_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: discounts_user
--

ALTER SEQUENCE public.account_skdkuser_groups_id_seq OWNED BY public.account_skdkuser_groups.id;


--
-- Name: account_skdkuser_id_seq; Type: SEQUENCE; Schema: public; Owner: discounts_user
--

CREATE SEQUENCE public.account_skdkuser_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_skdkuser_id_seq OWNER TO discounts_user;

--
-- Name: account_skdkuser_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: discounts_user
--

ALTER SEQUENCE public.account_skdkuser_id_seq OWNED BY public.account_skdkuser.id;


--
-- Name: account_skdkuser_user_permissions; Type: TABLE; Schema: public; Owner: discounts_user
--

CREATE TABLE public.account_skdkuser_user_permissions (
    id integer NOT NULL,
    skdkuser_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.account_skdkuser_user_permissions OWNER TO discounts_user;

--
-- Name: account_skdkuser_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: discounts_user
--

CREATE SEQUENCE public.account_skdkuser_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_skdkuser_user_permissions_id_seq OWNER TO discounts_user;

--
-- Name: account_skdkuser_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: discounts_user
--

ALTER SEQUENCE public.account_skdkuser_user_permissions_id_seq OWNED BY public.account_skdkuser_user_permissions.id;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: discounts_user
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO discounts_user;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: discounts_user
--

CREATE SEQUENCE public.auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO discounts_user;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: discounts_user
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: discounts_user
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO discounts_user;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: discounts_user
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO discounts_user;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: discounts_user
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: discounts_user
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO discounts_user;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: discounts_user
--

CREATE SEQUENCE public.auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO discounts_user;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: discounts_user
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: authtoken_token; Type: TABLE; Schema: public; Owner: discounts_user
--

CREATE TABLE public.authtoken_token (
    key character varying(40) NOT NULL,
    created timestamp with time zone NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.authtoken_token OWNER TO discounts_user;

--
-- Name: coupon_coupon; Type: TABLE; Schema: public; Owner: discounts_user
--

CREATE TABLE public.coupon_coupon (
    id integer NOT NULL,
    description character varying(1500) NOT NULL,
    percent character varying(3) NOT NULL,
    validity date,
    limited_quantity integer,
    coupon_balance integer,
    init_time date,
    activate boolean NOT NULL,
    repeatedly integer NOT NULL,
    discounter_id integer NOT NULL,
    CONSTRAINT coupon_coupon_coupon_balance_check CHECK ((coupon_balance >= 0)),
    CONSTRAINT coupon_coupon_limited_quantity_check CHECK ((limited_quantity >= 0)),
    CONSTRAINT coupon_coupon_repeatedly_check CHECK ((repeatedly >= 0))
);


ALTER TABLE public.coupon_coupon OWNER TO discounts_user;

--
-- Name: coupon_coupon_id_seq; Type: SEQUENCE; Schema: public; Owner: discounts_user
--

CREATE SEQUENCE public.coupon_coupon_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.coupon_coupon_id_seq OWNER TO discounts_user;

--
-- Name: coupon_coupon_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: discounts_user
--

ALTER SEQUENCE public.coupon_coupon_id_seq OWNED BY public.coupon_coupon.id;


--
-- Name: coupon_couponimage; Type: TABLE; Schema: public; Owner: discounts_user
--

CREATE TABLE public.coupon_couponimage (
    id integer NOT NULL,
    image character varying(100) NOT NULL,
    coupon_id integer NOT NULL
);


ALTER TABLE public.coupon_couponimage OWNER TO discounts_user;

--
-- Name: coupon_couponimage_id_seq; Type: SEQUENCE; Schema: public; Owner: discounts_user
--

CREATE SEQUENCE public.coupon_couponimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.coupon_couponimage_id_seq OWNER TO discounts_user;

--
-- Name: coupon_couponimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: discounts_user
--

ALTER SEQUENCE public.coupon_couponimage_id_seq OWNED BY public.coupon_couponimage.id;


--
-- Name: coupon_usagecoupon; Type: TABLE; Schema: public; Owner: discounts_user
--

CREATE TABLE public.coupon_usagecoupon (
    id integer NOT NULL,
    usage_date timestamp with time zone,
    code integer,
    client_id integer,
    usage_id integer,
    CONSTRAINT coupon_usagecoupon_code_check CHECK ((code >= 0))
);


ALTER TABLE public.coupon_usagecoupon OWNER TO discounts_user;

--
-- Name: coupon_usagecoupon_id_seq; Type: SEQUENCE; Schema: public; Owner: discounts_user
--

CREATE SEQUENCE public.coupon_usagecoupon_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.coupon_usagecoupon_id_seq OWNER TO discounts_user;

--
-- Name: coupon_usagecoupon_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: discounts_user
--

ALTER SEQUENCE public.coupon_usagecoupon_id_seq OWNED BY public.coupon_usagecoupon.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: discounts_user
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO discounts_user;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: discounts_user
--

CREATE SEQUENCE public.django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO discounts_user;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: discounts_user
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: discounts_user
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO discounts_user;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: discounts_user
--

CREATE SEQUENCE public.django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO discounts_user;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: discounts_user
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: discounts_user
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO discounts_user;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: discounts_user
--

CREATE SEQUENCE public.django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO discounts_user;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: discounts_user
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: discounts_user
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO discounts_user;

--
-- Name: django_summernote_attachment; Type: TABLE; Schema: public; Owner: discounts_user
--

CREATE TABLE public.django_summernote_attachment (
    id integer NOT NULL,
    name character varying(255),
    file character varying(100) NOT NULL,
    uploaded timestamp with time zone NOT NULL
);


ALTER TABLE public.django_summernote_attachment OWNER TO discounts_user;

--
-- Name: django_summernote_attachment_id_seq; Type: SEQUENCE; Schema: public; Owner: discounts_user
--

CREATE SEQUENCE public.django_summernote_attachment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_summernote_attachment_id_seq OWNER TO discounts_user;

--
-- Name: django_summernote_attachment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: discounts_user
--

ALTER SEQUENCE public.django_summernote_attachment_id_seq OWNED BY public.django_summernote_attachment.id;


--
-- Name: account_client_favorites id; Type: DEFAULT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_client_favorites ALTER COLUMN id SET DEFAULT nextval('public.account_client_favorites_id_seq'::regclass);


--
-- Name: account_discountbranch id; Type: DEFAULT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_discountbranch ALTER COLUMN id SET DEFAULT nextval('public.account_discountbranch_id_seq'::regclass);


--
-- Name: account_discountercategory id; Type: DEFAULT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_discountercategory ALTER COLUMN id SET DEFAULT nextval('public.account_discountercategory_id_seq'::regclass);


--
-- Name: account_discountercity id; Type: DEFAULT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_discountercity ALTER COLUMN id SET DEFAULT nextval('public.account_discountercity_id_seq'::regclass);


--
-- Name: account_emailactivation id; Type: DEFAULT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_emailactivation ALTER COLUMN id SET DEFAULT nextval('public.account_emailactivation_id_seq'::regclass);


--
-- Name: account_rate id; Type: DEFAULT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_rate ALTER COLUMN id SET DEFAULT nextval('public.account_rate_id_seq'::regclass);


--
-- Name: account_rateactivate id; Type: DEFAULT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_rateactivate ALTER COLUMN id SET DEFAULT nextval('public.account_rateactivate_id_seq'::regclass);


--
-- Name: account_skdkuser id; Type: DEFAULT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_skdkuser ALTER COLUMN id SET DEFAULT nextval('public.account_skdkuser_id_seq'::regclass);


--
-- Name: account_skdkuser_groups id; Type: DEFAULT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_skdkuser_groups ALTER COLUMN id SET DEFAULT nextval('public.account_skdkuser_groups_id_seq'::regclass);


--
-- Name: account_skdkuser_user_permissions id; Type: DEFAULT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_skdkuser_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.account_skdkuser_user_permissions_id_seq'::regclass);


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: coupon_coupon id; Type: DEFAULT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.coupon_coupon ALTER COLUMN id SET DEFAULT nextval('public.coupon_coupon_id_seq'::regclass);


--
-- Name: coupon_couponimage id; Type: DEFAULT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.coupon_couponimage ALTER COLUMN id SET DEFAULT nextval('public.coupon_couponimage_id_seq'::regclass);


--
-- Name: coupon_usagecoupon id; Type: DEFAULT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.coupon_usagecoupon ALTER COLUMN id SET DEFAULT nextval('public.coupon_usagecoupon_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: django_summernote_attachment id; Type: DEFAULT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.django_summernote_attachment ALTER COLUMN id SET DEFAULT nextval('public.django_summernote_attachment_id_seq'::regclass);


--
-- Data for Name: account_adminuser; Type: TABLE DATA; Schema: public; Owner: discounts_user
--

COPY public.account_adminuser (skdkuser_ptr_id) FROM stdin;
10
12
\.


--
-- Data for Name: account_client; Type: TABLE DATA; Schema: public; Owner: discounts_user
--

COPY public.account_client (skdkuser_ptr_id, avatar, gender, age, reset_key, born) FROM stdin;
5		0	\N	\N	2019-07-19 21:00:00+00
11		0	\N	\N	2019-07-19 21:00:00+00
27		0	\N	\N	2019-07-19 21:00:00+00
3		0	\N	\N	2019-07-19 21:00:00+00
28		0	\N	\N	2019-07-19 21:00:00+00
14		0	\N	\N	2019-07-19 21:00:00+00
15		0	\N	\N	2019-07-19 21:00:00+00
16		0	\N	\N	2019-07-19 21:00:00+00
17		0	\N	\N	2019-07-19 21:00:00+00
18		0	\N	\N	2019-07-19 21:00:00+00
21		0	\N	\N	2019-07-19 21:00:00+00
22		0	\N	\N	2019-07-19 21:00:00+00
19		0	\N	\N	2019-07-19 21:00:00+00
30		0	\N	\N	2019-07-19 21:00:00+00
33		0	\N	\N	2019-08-14 21:00:00+00
34		0	\N	\N	2019-08-14 21:00:00+00
35		0	\N	\N	2019-08-14 21:00:00+00
6		1	22	\N	2019-07-19 21:00:00+00
36		0	\N	\N	2019-09-09 21:00:00+00
\.


--
-- Data for Name: account_client_favorites; Type: TABLE DATA; Schema: public; Owner: discounts_user
--

COPY public.account_client_favorites (id, client_id, coupon_id) FROM stdin;
7	3	4
8	19	4
9	19	3
33	6	4
36	30	4
38	30	3
39	30	6
40	6	3
41	35	4
43	35	6
45	6	6
\.


--
-- Name: account_client_favorites_id_seq; Type: SEQUENCE SET; Schema: public; Owner: discounts_user
--

SELECT pg_catalog.setval('public.account_client_favorites_id_seq', 45, true);


--
-- Data for Name: account_discountbranch; Type: TABLE DATA; Schema: public; Owner: discounts_user
--

COPY public.account_discountbranch (id, avatar, name, description, address, parent_shop_id) FROM stdin;
\.


--
-- Name: account_discountbranch_id_seq; Type: SEQUENCE SET; Schema: public; Owner: discounts_user
--

SELECT pg_catalog.setval('public.account_discountbranch_id_seq', 1, true);


--
-- Data for Name: account_discounter; Type: TABLE DATA; Schema: public; Owner: discounts_user
--

COPY public.account_discounter (skdkuser_ptr_id, avatar, company_name, address, description, reset_key, category_id, city, country, hnum, street, "discounter_сity_id", close, opening, cover_image) FROM stdin;
31		\N	\N	\N	\N	\N					\N	11:13:51.501169	11:13:51.501155	\N
2	shilowstanislaw@gmail.com/shilowstanislaw@gmail.com_MDzgSwN.jpg	FoodHub	Санкт-Петербург Невский 121	бла-бла-бла	5i9cbaxgq7qalrco6hbnj7veuallkfpfcke6r	1	Санкт-Петербург	Россия	47	Невский проспект	1	02:00:00	09:00:00	shilowstanislaw@gmail.com/shilowstanislaw@gmail.com_UbYXjLm.jpg
\.


--
-- Data for Name: account_discountercategory; Type: TABLE DATA; Schema: public; Owner: discounts_user
--

COPY public.account_discountercategory (id, title, image) FROM stdin;
1	спорт	спорт/спорт_4rwhaPc.jpg
2	бары и рестораны	бары и рестораны/бары_и_рестораны_zVB1yTP.jpg
\.


--
-- Name: account_discountercategory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: discounts_user
--

SELECT pg_catalog.setval('public.account_discountercategory_id_seq', 2, true);


--
-- Data for Name: account_discountercity; Type: TABLE DATA; Schema: public; Owner: discounts_user
--

COPY public.account_discountercity (id, name) FROM stdin;
1	Санкт-Петербург
\.


--
-- Name: account_discountercity_id_seq; Type: SEQUENCE SET; Schema: public; Owner: discounts_user
--

SELECT pg_catalog.setval('public.account_discountercity_id_seq', 1, true);


--
-- Data for Name: account_emailactivation; Type: TABLE DATA; Schema: public; Owner: discounts_user
--

COPY public.account_emailactivation (id, email, key, activated, forced_expired, expires, "timestamp", update, user_id) FROM stdin;
1	shilowstanislaw@gmail.com	ka4uof9z6cypfsofpswy01b2so5jsbwdhhk54p6	t	f	7	2019-06-03 02:26:38.413404+00	2019-06-03 02:26:56.470271+00	2
4	stanislao.shilov@yandex.ru	f8uatbofxrmenedc1mtitiihfvy34qlpaagkyc	f	f	7	2019-07-28 11:13:51.56068+00	2019-07-28 11:13:51.560711+00	31
\.


--
-- Name: account_emailactivation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: discounts_user
--

SELECT pg_catalog.setval('public.account_emailactivation_id_seq', 4, true);


--
-- Data for Name: account_rate; Type: TABLE DATA; Schema: public; Owner: discounts_user
--

COPY public.account_rate (id, validity, name, description) FROM stdin;
1	6	пол года крутая погода	Продвигайте свою программу лояльности на протяжении полугода
2	1	пробный  месяц	попробуйте месяц пользования сервисом
3	3	Три Месяца	Бла-бла-бла
\.


--
-- Name: account_rate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: discounts_user
--

SELECT pg_catalog.setval('public.account_rate_id_seq', 3, true);


--
-- Data for Name: account_rateactivate; Type: TABLE DATA; Schema: public; Owner: discounts_user
--

COPY public.account_rateactivate (id, init_time, validity, status, rate_id, discounter_id) FROM stdin;
1	2019-06-03 02:32:29+00	2019-12-01 21:00:00+00	t	1	2
\.


--
-- Name: account_rateactivate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: discounts_user
--

SELECT pg_catalog.setval('public.account_rateactivate_id_seq', 2, true);


--
-- Data for Name: account_skdkuser; Type: TABLE DATA; Schema: public; Owner: discounts_user
--

COPY public.account_skdkuser (id, password, last_login, is_superuser, email, phone, first_name, last_name, dynamic_username, created_at, updated_at, is_staff, is_active) FROM stdin;
14	pbkdf2_sha256$36000$hBingOLJzMys$nal6tbZh4E9KZZ8CPMqdjjEgra3UFY0RAsTJFfNMGto=	\N	f	\N	+380954184765			+380954184765	2019-06-19 21:17:50.564437+00	2019-06-19 21:17:50.564453+00	f	t
30	pbkdf2_sha256$36000$QdZZAeLPkTog$9ZECsaYys2JClu8oybdnQjp0UjUizCraDzUce8RGdBE=	2019-08-21 10:04:14.109913+00	f	\N	+380638135751			+380638135751	2019-07-16 06:58:33.061234+00	2019-07-16 06:58:33.061251+00	f	t
15	pbkdf2_sha256$36000$RVL1ZkyqtkcM$aXtM/PT9s63HzkZDH6dRyJV3B99QXywkuQA6QHYPEKQ=	2019-06-19 21:33:53.915599+00	f	\N	+380635910955			+380635910955	2019-06-19 21:33:53.734261+00	2019-06-19 21:33:53.734274+00	f	t
33	pbkdf2_sha256$36000$38cWz3JoWrZC$+DWizGrUuuncpE6n3eFWdtsVZvUrl27x+eCcmL+mQQU=	2019-08-21 11:22:44.377316+00	f	\N	+79214078372			+79214078372	2019-08-21 11:22:12.096513+00	2019-08-21 11:22:12.09653+00	f	t
32	pbkdf2_sha256$36000$VX0AjYhHOl2i$AtlcdVieR9ci3/xbz23Q9tnUp1qMmDCMS38+1VsEsNc=	2019-08-01 17:20:38.775066+00	t	admin@skdk71.com	\N			admin@skdk71.com	2019-08-01 17:20:17.461585+00	2019-08-01 17:20:17.461606+00	t	t
2	pbkdf2_sha256$36000$SnNI4PES7rgq$y60p/JRrhQqA0dgWm5A5DdNfuJ0O1aPP1HqzPBhGfgE=	2019-08-01 17:27:06.394133+00	f	shilowstanislaw@gmail.com	+79914343465	Стас	Шилов	shilowstanislaw@gmail.com	2019-06-03 02:26:38+00	2019-06-03 02:26:38+00	f	t
34	pbkdf2_sha256$36000$T5hOEeynFqFL$lVQYqr7BPCErHskKGlww70YRZy7ipvCD6nmTFn1beQM=	2019-08-22 19:24:47.729073+00	f	\N	+79214078737			+79214078737	2019-08-22 19:23:31.927152+00	2019-08-22 19:23:31.927169+00	f	t
19	pbkdf2_sha256$36000$FiODGAPPgYQo$zhAS0P00x5gNA8eTsJWBK7nXnJw50y32whWa5ntdvEI=	2019-06-25 15:39:09.864757+00	f	\N	+79998371337			+79998371337	2019-06-23 17:20:27.653932+00	2019-06-23 17:20:27.653951+00	f	t
6	pbkdf2_sha256$36000$qLEmnZgISxuU$YRejQlsJs1rYt94uBkMEfyHUdivPXgyP0JtMDfwX82A=	2019-09-10 19:52:31.870825+00	f	iv.tkachenko@gmail.com	+380507017896	Ivan	Tkachenko	+380507017896	2019-06-04 20:44:11.484661+00	2019-06-04 20:44:11.48468+00	f	t
35	pbkdf2_sha256$36000$1N13WFRpB94L$WaiOTBORs4I2hFximf3C4druX1boDIFqvA/Ir96phzU=	2019-08-22 19:50:38.766488+00	f	\N	+380509151292			+380509151292	2019-08-22 19:30:11.175823+00	2019-08-22 19:30:11.175837+00	f	t
5	pbkdf2_sha256$36000$N5KqSvk5g1AD$pYnyoGLSewIqSdLXiLPyY1UmxsA29tgTtoB54ygw/AY=	2019-06-03 03:18:33+00	f	\N	+79819534345			+79819534345	2019-06-03 03:18:23.659775+00	2019-06-03 03:18:23.659791+00	f	t
36	pbkdf2_sha256$36000$O2pkhlyOPUcs$eAugzSYjpd1rEyQg9HXya+q5iPtWZY55lZQqL4nTEzA=	2019-09-11 06:54:17.592364+00	f	\N	+79534427854			+79534427854	2019-09-11 06:53:55.545429+00	2019-09-11 06:53:55.545447+00	f	t
10	pbkdf2_sha256$36000$LheiZOVPUoWK$80TE2nFSL/Jxkdpbb7UPQ/m68Cok9JDCEHXpE+4ohAY=	\N	f	vasya_admin@skdk71.com	\N	Василий	Васин	vasya_admin@skdk71.com	2019-06-09 11:04:07.823198+00	2019-06-09 11:04:07.823213+00	t	t
11	pbkdf2_sha256$36000$8S6KNJgAaIPm$I8OTmU89C7hqLN1qbe6hpX2JxL8CsKwc+ksbl9ODQtY=	2019-06-09 11:16:42.362582+00	f	\N	+79812234341			+79812234341	2019-06-09 11:16:29.802129+00	2019-06-09 11:16:29.80215+00	f	t
16	pbkdf2_sha256$36000$SRnJX6tGQvXB$TwO3tOfW1kWnlKU1mcVP6Kd7akP7orPwqMyF0eHRiu8=	\N	f	\N	+79819534344			+79819534344	2019-06-23 14:20:46.908559+00	2019-06-23 14:20:46.908582+00	f	t
17	pbkdf2_sha256$36000$Kn4VodPjyi0q$/yLTQ+h61aBit8ixQQyqzkEcpedzTD78OpEXhB7TlY8=	\N	f	\N	+798195343440			+798195343440	2019-06-23 14:24:02.26733+00	2019-06-23 14:24:02.26735+00	f	t
18	pbkdf2_sha256$36000$T8jSQfksnZrc$Bv9GVsDeui/pRCOAK3CBEuxDZ3adC7Frha5MomDlLZM=	\N	f	\N	+7981953434402			+7981953434402	2019-06-23 17:09:30.163877+00	2019-06-23 17:09:30.163901+00	f	t
1	pbkdf2_sha256$36000$zGIUBb1c0imO$AVNWgL+gh62AaKJVGMi+p2v5CSrrc2ZUGOT0FqGJdxE=	2019-07-16 17:06:40.392926+00	t	development.skidka71@gmail.com	\N			development.skidka71@gmail.com	2019-06-03 01:47:14.914427+00	2019-06-03 01:47:14.914452+00	t	t
21	pbkdf2_sha256$36000$uqlTUuRn1TVW$SjbzgWQWNjPYhz90QUN95G34nH6pWDbuk+8lpkSHGgc=	2019-06-23 18:28:41.070244+00	f	\N	+380979823929			+380979823929	2019-06-23 18:28:40.809077+00	2019-06-23 18:28:40.809089+00	f	t
22	pbkdf2_sha256$36000$aOYcVwX3w6Qj$hW7lMoorCwnoWwe0qU8c/OZgexEEU7MDZHfshFBRFpA=	2019-06-23 19:03:46+00	f	\N	+79814453434			+79814453434	2019-06-23 19:02:09+00	2019-06-23 19:02:09+00	f	t
12	pbkdf2_sha256$36000$G0z7U3vy5dAN$vK3YfZRyQYdD6LvOFu1d/IYZWvTI4oXiSg0qOqlb/q4=	2019-06-26 18:24:38.756545+00	f	info@alibi.com	\N			info@alibi.com	2019-06-17 08:14:00.734926+00	2019-06-17 08:14:00.734938+00	t	t
27	qwerty123	\N	f	\N	\N			+79999999999	2019-06-26 18:26:10+00	2019-06-26 18:26:10+00	f	t
31	pbkdf2_sha256$36000$FenppwPBYMW7$CC5emYIynH1OCMZys4quImj38B9FAxGWZYszSKwloc4=	\N	f	stanislao.shilov@yandex.ru	\N			stanislao.shilov@yandex.ru	2019-07-28 11:13:51.500946+00	2019-07-28 11:13:51.500969+00	f	f
3	pbkdf2_sha256$36000$RnQ3vVw9K0SD$EnyIdxaHeUD2uWJeyIAMnJ7CN9F8snphjDc+xjDmLjw=	2019-06-23 17:07:49+00	f	\N	+79819534340			+79819534340	2019-06-03 02:34:47+00	2019-06-03 02:34:47+00	f	t
28	pbkdf2_sha256$36000$YTl9TqvCtQmg$Ix7viKhe/5yjc7PfrzcrGY6/Qbr1OgZbLgD+ENdOG10=	2019-06-29 19:42:09.652585+00	f	\N	+79534422642			+79534422642	2019-06-28 17:20:48.432891+00	2019-06-28 17:20:48.432925+00	f	t
\.


--
-- Data for Name: account_skdkuser_groups; Type: TABLE DATA; Schema: public; Owner: discounts_user
--

COPY public.account_skdkuser_groups (id, skdkuser_id, group_id) FROM stdin;
1	12	1
\.


--
-- Name: account_skdkuser_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: discounts_user
--

SELECT pg_catalog.setval('public.account_skdkuser_groups_id_seq', 1, true);


--
-- Name: account_skdkuser_id_seq; Type: SEQUENCE SET; Schema: public; Owner: discounts_user
--

SELECT pg_catalog.setval('public.account_skdkuser_id_seq', 36, true);


--
-- Data for Name: account_skdkuser_user_permissions; Type: TABLE DATA; Schema: public; Owner: discounts_user
--

COPY public.account_skdkuser_user_permissions (id, skdkuser_id, permission_id) FROM stdin;
1	10	44
2	10	45
3	10	47
4	10	48
5	10	25
6	10	26
7	10	27
8	10	28
9	10	29
10	10	30
\.


--
-- Name: account_skdkuser_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: discounts_user
--

SELECT pg_catalog.setval('public.account_skdkuser_user_permissions_id_seq', 15, true);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: discounts_user
--

COPY public.auth_group (id, name) FROM stdin;
1	диспетчеры
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: discounts_user
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, true);


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: discounts_user
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
1	1	43
2	1	44
3	1	45
\.


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: discounts_user
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 3, true);


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: discounts_user
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can add permission	2	add_permission
5	Can change permission	2	change_permission
6	Can delete permission	2	delete_permission
7	Can add group	3	add_group
8	Can change group	3	change_group
9	Can delete group	3	delete_group
10	Can add content type	4	add_contenttype
11	Can change content type	4	change_contenttype
12	Can delete content type	4	delete_contenttype
13	Can add session	5	add_session
14	Can change session	5	change_session
15	Can delete session	5	delete_session
16	Can add attachment	6	add_attachment
17	Can change attachment	6	change_attachment
18	Can delete attachment	6	delete_attachment
19	Can add Token	7	add_token
20	Can change Token	7	change_token
21	Can delete Token	7	delete_token
22	Can add аккаунт	8	add_skdkuser
23	Can change аккаунт	8	change_skdkuser
24	Can delete аккаунт	8	delete_skdkuser
25	Can add филиал	9	add_discountbranch
26	Can change филиал	9	change_discountbranch
27	Can delete филиал	9	delete_discountbranch
28	Can add категория организации	10	add_discountercategory
29	Can change категория организации	10	change_discountercategory
30	Can delete категория организации	10	delete_discountercategory
31	Can add email activation	11	add_emailactivation
32	Can change email activation	11	change_emailactivation
33	Can delete email activation	11	delete_emailactivation
34	Can add тариф	12	add_rate
35	Can change тариф	12	change_rate
36	Can delete тариф	12	delete_rate
37	Can add активированный тариф	13	add_rateactivate
38	Can change активированный тариф	13	change_rateactivate
39	Can delete активированный тариф	13	delete_rateactivate
40	Can add администратор	14	add_adminuser
41	Can change администратор	14	change_adminuser
42	Can delete администратор	14	delete_adminuser
43	Can add клиент	15	add_client
44	Can change клиент	15	change_client
45	Can delete клиент	15	delete_client
46	Can add дискоунтер	16	add_discounter
47	Can change дискоунтер	16	change_discounter
48	Can delete дискоунтер	16	delete_discounter
49	Can add купон	17	add_coupon
50	Can change купон	17	change_coupon
51	Can delete купон	17	delete_coupon
52	Can add изображение	18	add_couponimage
53	Can change изображение	18	change_couponimage
54	Can delete изображение	18	delete_couponimage
55	Can add активация купона	19	add_usagecoupon
56	Can change активация купона	19	change_usagecoupon
57	Can delete активация купона	19	delete_usagecoupon
58	Can add discounter city	20	add_discountercity
59	Can change discounter city	20	change_discountercity
60	Can delete discounter city	20	delete_discountercity
\.


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: discounts_user
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 60, true);


--
-- Data for Name: authtoken_token; Type: TABLE DATA; Schema: public; Owner: discounts_user
--

COPY public.authtoken_token (key, created, user_id) FROM stdin;
dd790fc3c2002b125294107ba8ea4c5c21f9190c	2019-06-03 03:18:33.819093+00	5
578ab80f746d95e50bfbff83652946536cc1936f	2019-06-03 04:03:11.626648+00	3
3964af6a069412a35aea411b1135ef55e0b16f54	2019-06-09 11:16:42.357781+00	11
b8284cfba1bd676c1c06a0da92d666cce4610d87	2019-06-19 21:33:53.912848+00	15
c62c7bf6d5e18c95d60ae232136a985029b9455e	2019-06-23 17:20:27.874157+00	19
d555b7e6325a6789f0ce367a4bade10a64ebe266	2019-06-23 18:28:41.067959+00	21
248bc630e0500bf49144652044da82e8dd0aa479	2019-06-23 19:02:20.610541+00	22
44bb2553e7e6c0114b96d0555b3b75e8f86907d6	2019-06-28 17:21:30.246746+00	28
b9b4f29627a05b28c22cbe47c5c5f73e4bd097e0	2019-07-29 23:06:37.47941+00	2
5c800d293ea7d3ae7458210ae66be0ea95ef3b53	2019-08-22 19:50:38.762067+00	35
4f0849612cdd46a38265cfb3e6d36acef55285bc	2019-08-22 20:42:09.56102+00	6
201927d663206a7f21254fbe68da5647cdebbdbf	2019-09-11 06:54:17.582277+00	36
\.


--
-- Data for Name: coupon_coupon; Type: TABLE DATA; Schema: public; Owner: discounts_user
--

COPY public.coupon_coupon (id, description, percent, validity, limited_quantity, coupon_balance, init_time, activate, repeatedly, discounter_id) FROM stdin;
4	скидка на корпоратив	20%	2019-12-31	\N	\N	2019-06-03	t	3	2
3	скидка на пиццу после 21:00	30	2019-11-14	2	0	2019-06-03	t	3	2
6	купон спецом для IvanIOS	100	2919-07-16	\N	\N	2019-07-16	t	100	2
\.


--
-- Name: coupon_coupon_id_seq; Type: SEQUENCE SET; Schema: public; Owner: discounts_user
--

SELECT pg_catalog.setval('public.coupon_coupon_id_seq', 6, true);


--
-- Data for Name: coupon_couponimage; Type: TABLE DATA; Schema: public; Owner: discounts_user
--

COPY public.coupon_couponimage (id, image, coupon_id) FROM stdin;
4	shilowstanislaw@gmail.com/скидка на пиццу после 21:00/e765nr.jpg	3
5	shilowstanislaw@gmail.com/скидка на пиццу после 21:00/cukynl.jpg	3
6	shilowstanislaw@gmail.com/скидка на пиццу после 21:00/la76x5.jpg	3
7	shilowstanislaw@gmail.com/скидка на корпоратив/gs8te1.jpg	4
\.


--
-- Name: coupon_couponimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: discounts_user
--

SELECT pg_catalog.setval('public.coupon_couponimage_id_seq', 12, true);


--
-- Data for Name: coupon_usagecoupon; Type: TABLE DATA; Schema: public; Owner: discounts_user
--

COPY public.coupon_usagecoupon (id, usage_date, code, client_id, usage_id) FROM stdin;
1	2019-06-03 04:03:43.060761+00	108367952	3	3
2	2019-06-03 04:03:47.894684+00	347061258	3	3
3	2019-06-03 04:03:49.099587+00	435679082	3	3
4	2019-06-04 17:14:51.216912+00	307145692	3	4
5	2019-07-16 20:45:14.912107+00	462985370	6	4
6	2019-07-16 21:12:48.196445+00	813750496	6	4
7	2019-07-16 21:14:44.202576+00	71496528	6	4
8	2019-07-16 21:18:31.331029+00	254398670	6	6
9	2019-07-16 21:50:47.117121+00	830657149	6	6
12	2019-07-17 05:29:31.304424+00	804169573	6	6
13	2019-07-17 06:09:23.910609+00	649108352	6	6
14	2019-07-17 06:12:04.242954+00	861972053	6	6
15	2019-07-17 06:13:42.380336+00	834126975	6	6
16	2019-07-17 06:14:05.815841+00	870246951	6	6
17	2019-07-17 06:17:10.859896+00	560832974	6	6
18	2019-07-17 06:17:34.631588+00	981372456	6	6
19	2019-07-17 06:17:39.307151+00	208467153	6	6
20	2019-07-17 06:28:26.404179+00	967021835	6	6
21	2019-07-17 06:32:44.030503+00	370459268	6	6
22	2019-07-17 06:35:07.991174+00	94872315	6	6
23	2019-07-17 06:36:52.784238+00	147092563	6	6
24	2019-07-17 06:44:37.797756+00	45987261	6	6
25	2019-07-17 06:46:31.084253+00	601738492	6	6
26	2019-07-17 07:26:56.872286+00	572183609	6	6
27	2019-07-17 07:33:29.50797+00	721804395	6	6
28	2019-07-17 12:41:07.871954+00	356149702	6	6
29	2019-07-17 12:51:42.915705+00	610392854	6	6
30	2019-07-17 12:52:06.681218+00	304698175	6	6
31	2019-07-17 12:54:09.879054+00	49736821	6	6
32	2019-07-17 12:54:45.044947+00	683027194	6	6
33	2019-07-17 12:57:33.797288+00	619425730	6	6
34	2019-07-17 12:58:18.571487+00	519832476	6	6
35	2019-07-17 13:28:39.101207+00	972810365	6	6
36	2019-07-27 15:39:31.927725+00	130927864	6	6
37	2019-07-27 15:40:29.971131+00	709425618	6	6
38	2019-07-29 20:47:35.169519+00	68347912	6	6
39	2019-07-30 21:23:24.019754+00	705142368	6	3
40	2019-07-30 21:25:05.680404+00	379108624	6	3
41	2019-07-30 21:31:58.887694+00	760132948	6	6
42	2019-07-30 22:03:57.867513+00	219365047	6	6
43	2019-08-01 19:57:03.963087+00	476538012	30	3
44	2019-08-01 19:58:07.907647+00	842607351	30	3
45	2019-08-08 15:23:35.049236+00	435176982	30	3
46	2019-08-17 13:47:29.096316+00	805267934	6	3
47	2019-08-22 19:55:03.391453+00	706243815	35	4
48	2019-08-22 19:56:48.121557+00	402379651	35	6
\.


--
-- Name: coupon_usagecoupon_id_seq; Type: SEQUENCE SET; Schema: public; Owner: discounts_user
--

SELECT pg_catalog.setval('public.coupon_usagecoupon_id_seq', 48, true);


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: discounts_user
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2019-06-03 02:21:06.703194+00	1	спорт	1	[{"added": {}}]	10	1
2	2019-06-03 02:21:20.061439+00	2	бары и рестораны	1	[{"added": {}}]	10	1
3	2019-06-03 02:21:59.048379+00	1	пол года крутая погода	1	[{"added": {}}]	12	1
4	2019-06-03 02:22:38.625035+00	2	пробный  месяц	1	[{"added": {}}]	12	1
5	2019-06-03 02:32:43.847143+00	2	shilowstanislaw@gmail.com, Стас, foodhub	2	[{"added": {"name": "\\u0430\\u043a\\u0442\\u0438\\u0432\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u043d\\u044b\\u0439 \\u0442\\u0430\\u0440\\u0438\\u0444", "object": "2019-06-03 05:32:29+03:00, None"}}]	16	1
6	2019-06-03 02:45:08.136925+00	2	shilowstanislaw@gmail.com, Стас, foodhub	2	[{"changed": {"name": "\\u043a\\u0443\\u043f\\u043e\\u043d", "object": "foodhub, \\u0441\\u043a\\u0438\\u0434\\u043a\\u0430 \\u043d\\u0430 \\u043f\\u0438\\u0446\\u0446\\u0443 \\u043f\\u043e\\u0441\\u043b\\u0435 21:00", "fields": ["coupon_balance"]}}]	16	1
7	2019-06-03 02:56:40.760746+00	2	shilowstanislaw@gmail.com, Стас, foodhub	2	[{"changed": {"name": "\\u043a\\u0443\\u043f\\u043e\\u043d", "object": "foodhub, \\u0441\\u043a\\u0438\\u0434\\u043a\\u0430 \\u043d\\u0430 \\u043f\\u0438\\u0446\\u0446\\u0443 \\u043f\\u043e\\u0441\\u043b\\u0435 21:00", "fields": ["coupon_balance"]}}]	16	1
8	2019-06-03 03:07:13.418252+00	2	shilowstanislaw@gmail.com, Стас, foodhub	2	[{"changed": {"name": "\\u043a\\u0443\\u043f\\u043e\\u043d", "object": "foodhub, \\u0441\\u043a\\u0438\\u0434\\u043a\\u0430 \\u043d\\u0430 \\u043f\\u0438\\u0446\\u0446\\u0443 \\u043f\\u043e\\u0441\\u043b\\u0435 21:00", "fields": ["coupon_balance"]}}]	16	1
9	2019-06-06 09:44:53.459002+00	7	stanislao.shilov@yandex.ru, , None	3		16	1
10	2019-06-06 09:46:15.813911+00	8	stanislao.shilov@yandex.ru, , None	2	[{"added": {"name": "\\u0430\\u043a\\u0442\\u0438\\u0432\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u043d\\u044b\\u0439 \\u0442\\u0430\\u0440\\u0438\\u0444", "object": "2019-06-06 12:46:05+03:00, None"}}]	16	1
11	2019-06-07 06:57:16.13042+00	9	89534422642,	1	[{"added": {}}]	15	1
12	2019-06-07 06:58:16.6833+00	9	89534422642,	3		15	1
13	2019-06-09 11:02:51.256928+00	2	shilowstanislaw@gmail.com, +79819534348	2	[{"changed": {"fields": ["is_staff"]}}]	8	1
14	2019-06-09 11:03:02.375477+00	8	stanislao.shilov@yandex.ru, +79819534351	2	[{"changed": {"fields": ["is_staff"]}}]	8	1
15	2019-06-09 11:03:12.350826+00	6	+380507017896, +380507017896	2	[{"changed": {"fields": ["is_staff"]}}]	8	1
16	2019-06-09 11:03:22.047795+00	3	+79819534340, +79819534340	2	[{"changed": {"fields": ["is_staff"]}}]	8	1
17	2019-06-09 11:03:30.510875+00	5	+79819534345, +79819534345	2	[{"changed": {"fields": ["is_staff"]}}]	8	1
18	2019-06-09 11:03:40.21054+00	4	+79819534346, +79819534346	2	[{"changed": {"fields": ["is_staff"]}}]	8	1
19	2019-06-09 11:04:07.869993+00	10	vasya_admin@skdk71.com, None	1	[{"added": {}}]	14	1
20	2019-06-09 11:05:02.659713+00	10	vasya_admin@skdk71.com, None	2	[{"changed": {"fields": ["first_name", "last_name", "is_staff", "user_permissions"]}}]	14	1
21	2019-06-17 08:14:00.820711+00	12	info@alibi.com, None	1	[{"added": {}}]	14	1
22	2019-06-17 08:15:44.136712+00	12	info@alibi.com, None	2	[{"changed": {"fields": ["is_staff", "user_permissions"]}}]	14	1
23	2019-06-17 08:16:46.170488+00	1	диспетчеры	1	[{"added": {}}]	3	1
24	2019-06-17 08:17:22.353163+00	12	info@alibi.com, None	2	[{"changed": {"fields": ["groups", "user_permissions"]}}]	14	1
25	2019-06-17 08:30:23.580065+00	3	Три Месяца	1	[{"added": {}}]	12	1
26	2019-06-19 08:17:49.721572+00	13	+380638135751,	1	[{"added": {}}]	15	1
27	2019-06-19 08:18:58.394776+00	13	+380638135751,	2	[{"changed": {"fields": ["email", "dynamic_username"]}}]	15	1
28	2019-06-19 08:20:30.863364+00	13	+380638135751,	2	[]	15	1
29	2019-06-19 08:21:36.643793+00	13	+380638135751,	2	[{"changed": {"fields": ["email"]}}]	15	1
30	2019-06-19 08:53:39.815298+00	13	+380638135751,	2	[]	15	1
31	2019-06-23 17:35:45.71954+00	13	+380638135751,	3		15	1
32	2019-06-23 17:51:55.461472+00	20	+380638135751,	2	[{"changed": {"fields": ["email"]}}]	15	1
33	2019-06-23 18:17:59.136034+00	2	None, Стас, foodhub	2	[{"changed": {"fields": ["email"]}}]	16	1
34	2019-06-23 18:18:12.452328+00	3	+79819534340,	2	[{"changed": {"fields": ["email"]}}]	15	1
35	2019-06-23 18:20:22.695921+00	3	+79819534340,	2	[{"changed": {"fields": ["email"]}}]	15	1
36	2019-06-23 18:20:40.363932+00	2	shilowstanislaw@gmail.com, Стас, foodhub	2	[{"changed": {"fields": ["email"]}}]	16	1
37	2019-06-23 18:20:49.148698+00	8	None, Стас, жратушки	2	[{"changed": {"fields": ["email"]}}, {"changed": {"name": "\\u0444\\u0438\\u043b\\u0438\\u0430\\u043b", "object": "Branch, \\u041c\\u043e\\u0441\\u043a\\u0432\\u0430 \\u043f\\u0435\\u0440. \\u0410\\u0434\\u0440\\u0435\\u0441\\u043d\\u044b\\u0439 \\u0434. 32", "fields": ["description"]}}]	16	1
38	2019-06-23 18:22:47.142709+00	3	+79819534340,	2	[{"changed": {"fields": ["email"]}}]	15	1
39	2019-06-23 19:02:34.395449+00	3	+79819534340,	2	[{"changed": {"fields": ["last_login", "email"]}}]	15	1
40	2019-06-23 19:02:43.74379+00	22	+79814453434,	2	[{"changed": {"fields": ["email"]}}]	15	1
41	2019-06-23 21:50:30.518792+00	22	+79814453434,	2	[{"changed": {"fields": ["email"]}}]	15	1
42	2019-06-23 21:50:42.122397+00	8	stanislao.shilov@yandex.ru, Стас, жратушки	2	[{"changed": {"fields": ["email"]}}]	16	1
43	2019-06-26 18:26:44.616486+00	27	None,	1	[{"added": {}}]	15	12
44	2019-06-27 03:02:56.509656+00	3	+79819534340,	2	[]	15	1
45	2019-06-29 07:59:44.066625+00	4	+79819534346,	3		15	1
46	2019-06-29 08:26:04.78409+00	26	+79056248508,	3		15	1
47	2019-06-29 08:26:04.790755+00	25	+79534427854,	3		15	1
48	2019-06-29 08:26:04.80633+00	24	+79051198631,	3		15	1
49	2019-06-29 08:26:16.53059+00	23	+380632551225,	3		15	1
50	2019-06-29 08:30:47.639602+00	20	+380638135751,	3		15	1
51	2019-06-29 08:35:02.005278+00	29	+380638135751,	3		15	1
52	2019-07-16 17:09:21.133132+00	2	shilowstanislaw@gmail.com, Стас, foodhub	2	[{"added": {"name": "\\u043a\\u0443\\u043f\\u043e\\u043d", "object": "foodhub, \\u043a\\u0443\\u043f\\u043e\\u043d \\u0441\\u043f\\u0435\\u0446\\u043e\\u043c \\u0434\\u043b\\u044f IvanIOS"}}]	16	1
53	2019-07-26 01:27:17.964934+00	8	stanislao.shilov@yandex.ru, Стас, жратушки	2	[{"deleted": {"name": "\\u043a\\u0443\\u043f\\u043e\\u043d", "object": "\\u0436\\u0440\\u0430\\u0442\\u0443\\u0448\\u043a\\u0438, \\u043f\\u0440\\u043e\\u0441\\u0442\\u043e \\u0441\\u043a\\u0438\\u0434\\u043e\\u043d"}}, {"deleted": {"name": "\\u0444\\u0438\\u043b\\u0438\\u0430\\u043b", "object": "Branch, \\u041c\\u043e\\u0441\\u043a\\u0432\\u0430 \\u043f\\u0435\\u0440. \\u0410\\u0434\\u0440\\u0435\\u0441\\u043d\\u044b\\u0439 \\u0434. 32"}}]	16	1
54	2019-07-26 01:27:30.966467+00	8	stanislao.shilov@yandex.ru, Стас, жратушки	3		16	1
\.


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: discounts_user
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 54, true);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: discounts_user
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	contenttypes	contenttype
5	sessions	session
6	django_summernote	attachment
7	authtoken	token
8	account	skdkuser
9	account	discountbranch
10	account	discountercategory
11	account	emailactivation
12	account	rate
13	account	rateactivate
14	account	adminuser
15	account	client
16	account	discounter
17	coupon	coupon
18	coupon	couponimage
19	coupon	usagecoupon
20	account	discountercity
\.


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: discounts_user
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 20, true);


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: discounts_user
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2019-06-03 01:46:49.458809+00
2	contenttypes	0002_remove_content_type_name	2019-06-03 01:46:49.487558+00
3	auth	0001_initial	2019-06-03 01:46:49.759819+00
4	auth	0002_alter_permission_name_max_length	2019-06-03 01:46:49.780588+00
5	auth	0003_alter_user_email_max_length	2019-06-03 01:46:49.800222+00
6	auth	0004_alter_user_username_opts	2019-06-03 01:46:49.81247+00
7	auth	0005_alter_user_last_login_null	2019-06-03 01:46:49.8299+00
8	auth	0006_require_contenttypes_0002	2019-06-03 01:46:49.834218+00
9	auth	0007_alter_validators_add_error_messages	2019-06-03 01:46:49.889232+00
10	auth	0008_alter_user_username_max_length	2019-06-03 01:46:49.898011+00
11	account	0001_initial	2019-06-03 01:46:50.188406+00
12	coupon	0001_initial	2019-06-03 01:46:50.302131+00
13	account	0002_client_favorites	2019-06-03 01:46:50.345766+00
14	admin	0001_initial	2019-06-03 01:46:50.410042+00
15	admin	0002_logentry_remove_auto_add	2019-06-03 01:46:50.443745+00
16	authtoken	0001_initial	2019-06-03 01:46:50.491832+00
17	authtoken	0002_auto_20160226_1747	2019-06-03 01:46:50.587055+00
18	django_summernote	0001_initial	2019-06-03 01:46:50.601535+00
19	django_summernote	0002_update-help_text	2019-06-03 01:46:50.609973+00
20	sessions	0001_initial	2019-06-03 01:46:50.639259+00
21	coupon	0002_auto_20190603_0534	2019-06-03 02:34:17.219073+00
22	account	0003_auto_20190609_1401	2019-06-09 11:01:51.699184+00
23	account	0004_auto_20190720_0406	2019-07-20 01:06:20.288422+00
24	account	0005_auto_20190725_2008	2019-07-25 17:09:01.886272+00
25	account	0006_auto_20190726_0423	2019-07-26 01:23:45.728707+00
26	coupon	0003_auto_20190726_0426	2019-07-26 01:27:06.6331+00
27	account	0007_auto_20190726_1417	2019-07-26 11:17:09.433916+00
28	account	0008_auto_20190801_1950	2019-08-01 16:50:36.869716+00
29	account	0009_auto_20190801_2045	2019-08-01 17:45:47.287653+00
\.


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: discounts_user
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 29, true);


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: discounts_user
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
wscnw9qqjs2t5oi49q47r26ezdnbiaqj	ODBhZDcxYWQ4ZWNlMGEwYjJjNmM4Nzc1NzFiOTFkMzU2NGJhMjc5Mjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5BbGxvd0FsbFVzZXJzTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYWI2YjJkMzRlMTU0OWY0MTgyNjc2MDBlMWE2ZGM2ZjU2Zjc3YjdkOSJ9	2019-06-17 19:07:44.865754+00
qd76u1ccinh1qoxmujg5thz7m77cleid	ODBhZDcxYWQ4ZWNlMGEwYjJjNmM4Nzc1NzFiOTFkMzU2NGJhMjc5Mjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5BbGxvd0FsbFVzZXJzTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYWI2YjJkMzRlMTU0OWY0MTgyNjc2MDBlMWE2ZGM2ZjU2Zjc3YjdkOSJ9	2019-06-20 17:28:35.359551+00
4hssa85rr5r258ohvtdmqq86asghzom4	ODBhZDcxYWQ4ZWNlMGEwYjJjNmM4Nzc1NzFiOTFkMzU2NGJhMjc5Mjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5BbGxvd0FsbFVzZXJzTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYWI2YjJkMzRlMTU0OWY0MTgyNjc2MDBlMWE2ZGM2ZjU2Zjc3YjdkOSJ9	2019-06-23 10:58:14.566682+00
1bak0wlb6yeb28vbvr7xlgl4ntn8yzh0	ODBhZDcxYWQ4ZWNlMGEwYjJjNmM4Nzc1NzFiOTFkMzU2NGJhMjc5Mjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5BbGxvd0FsbFVzZXJzTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYWI2YjJkMzRlMTU0OWY0MTgyNjc2MDBlMWE2ZGM2ZjU2Zjc3YjdkOSJ9	2019-07-01 08:19:39.660504+00
wf529a1f9qdxh9s78sksp5k0e3avkyu2	ODBhZDcxYWQ4ZWNlMGEwYjJjNmM4Nzc1NzFiOTFkMzU2NGJhMjc5Mjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5BbGxvd0FsbFVzZXJzTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYWI2YjJkMzRlMTU0OWY0MTgyNjc2MDBlMWE2ZGM2ZjU2Zjc3YjdkOSJ9	2019-07-07 14:15:22.547241+00
pu7ergq68tygdq8koyalweghst1elkdc	ODBhZDcxYWQ4ZWNlMGEwYjJjNmM4Nzc1NzFiOTFkMzU2NGJhMjc5Mjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5BbGxvd0FsbFVzZXJzTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYWI2YjJkMzRlMTU0OWY0MTgyNjc2MDBlMWE2ZGM2ZjU2Zjc3YjdkOSJ9	2019-07-09 13:15:05.351108+00
8tw6tbd761kd43bl7mt65z7gqtzqvs0v	MzBlODUzNjk2MTU1ZmQ1MzMxMWZiOTNhOGY4MTAxYmI2NDdiNjZiODp7Il9hdXRoX3VzZXJfaWQiOiIxMiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuQWxsb3dBbGxVc2Vyc01vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaGFzaCI6IjQxNjViNzg0N2NkZTg1MWM3ZWE1NzJjZDA4MTIyN2U4OTgyZWZmMzgifQ==	2019-07-10 18:24:38.760054+00
xw1emfzmjlry0r1emgcc1hptemc02vf4	ODBhZDcxYWQ4ZWNlMGEwYjJjNmM4Nzc1NzFiOTFkMzU2NGJhMjc5Mjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5BbGxvd0FsbFVzZXJzTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYWI2YjJkMzRlMTU0OWY0MTgyNjc2MDBlMWE2ZGM2ZjU2Zjc3YjdkOSJ9	2019-07-13 08:25:00.579335+00
3dx207668g0dcpa4l3pi7yps2lilsul1	ODBhZDcxYWQ4ZWNlMGEwYjJjNmM4Nzc1NzFiOTFkMzU2NGJhMjc5Mjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5BbGxvd0FsbFVzZXJzTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYWI2YjJkMzRlMTU0OWY0MTgyNjc2MDBlMWE2ZGM2ZjU2Zjc3YjdkOSJ9	2019-07-30 17:06:40.397043+00
uarahaulcn76v35wnyw95p27dldzaqs1	NjFjMjUxM2IxNmZmNTAwZTJlNjY5MDI3NTFlYWU2MjcyNjc2NGNjMzp7Il9hdXRoX3VzZXJfaWQiOiIzMiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuQWxsb3dBbGxVc2Vyc01vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaGFzaCI6IjM3MmNiNjNjZTZmZjA4ZDdkNWI2YjY3MDlmNDc1Y2M2ZWFiNmRjMDcifQ==	2019-08-15 17:20:38.786604+00
\.


--
-- Data for Name: django_summernote_attachment; Type: TABLE DATA; Schema: public; Owner: discounts_user
--

COPY public.django_summernote_attachment (id, name, file, uploaded) FROM stdin;
\.


--
-- Name: django_summernote_attachment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: discounts_user
--

SELECT pg_catalog.setval('public.django_summernote_attachment_id_seq', 1, false);


--
-- Name: account_adminuser account_adminuser_pkey; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_adminuser
    ADD CONSTRAINT account_adminuser_pkey PRIMARY KEY (skdkuser_ptr_id);


--
-- Name: account_client_favorites account_client_favorites_client_id_coupon_id_fade971e_uniq; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_client_favorites
    ADD CONSTRAINT account_client_favorites_client_id_coupon_id_fade971e_uniq UNIQUE (client_id, coupon_id);


--
-- Name: account_client_favorites account_client_favorites_pkey; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_client_favorites
    ADD CONSTRAINT account_client_favorites_pkey PRIMARY KEY (id);


--
-- Name: account_client account_client_pkey; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_client
    ADD CONSTRAINT account_client_pkey PRIMARY KEY (skdkuser_ptr_id);


--
-- Name: account_discountbranch account_discountbranch_pkey; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_discountbranch
    ADD CONSTRAINT account_discountbranch_pkey PRIMARY KEY (id);


--
-- Name: account_discounter account_discounter_pkey; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_discounter
    ADD CONSTRAINT account_discounter_pkey PRIMARY KEY (skdkuser_ptr_id);


--
-- Name: account_discountercategory account_discountercategory_pkey; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_discountercategory
    ADD CONSTRAINT account_discountercategory_pkey PRIMARY KEY (id);


--
-- Name: account_discountercity account_discountercity_pkey; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_discountercity
    ADD CONSTRAINT account_discountercity_pkey PRIMARY KEY (id);


--
-- Name: account_emailactivation account_emailactivation_pkey; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_emailactivation
    ADD CONSTRAINT account_emailactivation_pkey PRIMARY KEY (id);


--
-- Name: account_rate account_rate_pkey; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_rate
    ADD CONSTRAINT account_rate_pkey PRIMARY KEY (id);


--
-- Name: account_rateactivate account_rateactivate_pkey; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_rateactivate
    ADD CONSTRAINT account_rateactivate_pkey PRIMARY KEY (id);


--
-- Name: account_skdkuser account_skdkuser_dynamic_username_key; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_skdkuser
    ADD CONSTRAINT account_skdkuser_dynamic_username_key UNIQUE (dynamic_username);


--
-- Name: account_skdkuser account_skdkuser_email_key; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_skdkuser
    ADD CONSTRAINT account_skdkuser_email_key UNIQUE (email);


--
-- Name: account_skdkuser_groups account_skdkuser_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_skdkuser_groups
    ADD CONSTRAINT account_skdkuser_groups_pkey PRIMARY KEY (id);


--
-- Name: account_skdkuser_groups account_skdkuser_groups_skdkuser_id_group_id_b64d9c0e_uniq; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_skdkuser_groups
    ADD CONSTRAINT account_skdkuser_groups_skdkuser_id_group_id_b64d9c0e_uniq UNIQUE (skdkuser_id, group_id);


--
-- Name: account_skdkuser account_skdkuser_phone_key; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_skdkuser
    ADD CONSTRAINT account_skdkuser_phone_key UNIQUE (phone);


--
-- Name: account_skdkuser account_skdkuser_pkey; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_skdkuser
    ADD CONSTRAINT account_skdkuser_pkey PRIMARY KEY (id);


--
-- Name: account_skdkuser_user_permissions account_skdkuser_user_pe_skdkuser_id_permission_i_341de251_uniq; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_skdkuser_user_permissions
    ADD CONSTRAINT account_skdkuser_user_pe_skdkuser_id_permission_i_341de251_uniq UNIQUE (skdkuser_id, permission_id);


--
-- Name: account_skdkuser_user_permissions account_skdkuser_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_skdkuser_user_permissions
    ADD CONSTRAINT account_skdkuser_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: authtoken_token authtoken_token_pkey; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_pkey PRIMARY KEY (key);


--
-- Name: authtoken_token authtoken_token_user_id_key; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_user_id_key UNIQUE (user_id);


--
-- Name: coupon_coupon coupon_coupon_pkey; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.coupon_coupon
    ADD CONSTRAINT coupon_coupon_pkey PRIMARY KEY (id);


--
-- Name: coupon_couponimage coupon_couponimage_pkey; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.coupon_couponimage
    ADD CONSTRAINT coupon_couponimage_pkey PRIMARY KEY (id);


--
-- Name: coupon_usagecoupon coupon_usagecoupon_pkey; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.coupon_usagecoupon
    ADD CONSTRAINT coupon_usagecoupon_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: django_summernote_attachment django_summernote_attachment_pkey; Type: CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.django_summernote_attachment
    ADD CONSTRAINT django_summernote_attachment_pkey PRIMARY KEY (id);


--
-- Name: account_client_favorites_client_id_46fe6068; Type: INDEX; Schema: public; Owner: discounts_user
--

CREATE INDEX account_client_favorites_client_id_46fe6068 ON public.account_client_favorites USING btree (client_id);


--
-- Name: account_client_favorites_coupon_id_59a6b363; Type: INDEX; Schema: public; Owner: discounts_user
--

CREATE INDEX account_client_favorites_coupon_id_59a6b363 ON public.account_client_favorites USING btree (coupon_id);


--
-- Name: account_discountbranch_parent_shop_id_faec958c; Type: INDEX; Schema: public; Owner: discounts_user
--

CREATE INDEX account_discountbranch_parent_shop_id_faec958c ON public.account_discountbranch USING btree (parent_shop_id);


--
-- Name: account_discounter_category_id_d609d94c; Type: INDEX; Schema: public; Owner: discounts_user
--

CREATE INDEX account_discounter_category_id_d609d94c ON public.account_discounter USING btree (category_id);


--
-- Name: account_discounter_discounter_сity_id_65fc0bd7; Type: INDEX; Schema: public; Owner: discounts_user
--

CREATE INDEX "account_discounter_discounter_сity_id_65fc0bd7" ON public.account_discounter USING btree ("discounter_сity_id");


--
-- Name: account_emailactivation_user_id_acd8adb2; Type: INDEX; Schema: public; Owner: discounts_user
--

CREATE INDEX account_emailactivation_user_id_acd8adb2 ON public.account_emailactivation USING btree (user_id);


--
-- Name: account_rateactivate_discounter_id_82b1a488; Type: INDEX; Schema: public; Owner: discounts_user
--

CREATE INDEX account_rateactivate_discounter_id_82b1a488 ON public.account_rateactivate USING btree (discounter_id);


--
-- Name: account_rateactivate_rate_id_eca1f8ed; Type: INDEX; Schema: public; Owner: discounts_user
--

CREATE INDEX account_rateactivate_rate_id_eca1f8ed ON public.account_rateactivate USING btree (rate_id);


--
-- Name: account_skdkuser_dynamic_username_ae318aeb_like; Type: INDEX; Schema: public; Owner: discounts_user
--

CREATE INDEX account_skdkuser_dynamic_username_ae318aeb_like ON public.account_skdkuser USING btree (dynamic_username varchar_pattern_ops);


--
-- Name: account_skdkuser_email_2c0eb0bb_like; Type: INDEX; Schema: public; Owner: discounts_user
--

CREATE INDEX account_skdkuser_email_2c0eb0bb_like ON public.account_skdkuser USING btree (email varchar_pattern_ops);


--
-- Name: account_skdkuser_groups_group_id_79b2fcb5; Type: INDEX; Schema: public; Owner: discounts_user
--

CREATE INDEX account_skdkuser_groups_group_id_79b2fcb5 ON public.account_skdkuser_groups USING btree (group_id);


--
-- Name: account_skdkuser_groups_skdkuser_id_173fabb4; Type: INDEX; Schema: public; Owner: discounts_user
--

CREATE INDEX account_skdkuser_groups_skdkuser_id_173fabb4 ON public.account_skdkuser_groups USING btree (skdkuser_id);


--
-- Name: account_skdkuser_phone_82e5dee5_like; Type: INDEX; Schema: public; Owner: discounts_user
--

CREATE INDEX account_skdkuser_phone_82e5dee5_like ON public.account_skdkuser USING btree (phone varchar_pattern_ops);


--
-- Name: account_skdkuser_user_permissions_permission_id_3a480410; Type: INDEX; Schema: public; Owner: discounts_user
--

CREATE INDEX account_skdkuser_user_permissions_permission_id_3a480410 ON public.account_skdkuser_user_permissions USING btree (permission_id);


--
-- Name: account_skdkuser_user_permissions_skdkuser_id_81982d43; Type: INDEX; Schema: public; Owner: discounts_user
--

CREATE INDEX account_skdkuser_user_permissions_skdkuser_id_81982d43 ON public.account_skdkuser_user_permissions USING btree (skdkuser_id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: discounts_user
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: discounts_user
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: discounts_user
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: discounts_user
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: authtoken_token_key_10f0b77e_like; Type: INDEX; Schema: public; Owner: discounts_user
--

CREATE INDEX authtoken_token_key_10f0b77e_like ON public.authtoken_token USING btree (key varchar_pattern_ops);


--
-- Name: coupon_coupon_discounter_id_24517252; Type: INDEX; Schema: public; Owner: discounts_user
--

CREATE INDEX coupon_coupon_discounter_id_24517252 ON public.coupon_coupon USING btree (discounter_id);


--
-- Name: coupon_couponimage_coupon_id_1d9a4695; Type: INDEX; Schema: public; Owner: discounts_user
--

CREATE INDEX coupon_couponimage_coupon_id_1d9a4695 ON public.coupon_couponimage USING btree (coupon_id);


--
-- Name: coupon_usagecoupon_client_id_92844339; Type: INDEX; Schema: public; Owner: discounts_user
--

CREATE INDEX coupon_usagecoupon_client_id_92844339 ON public.coupon_usagecoupon USING btree (client_id);


--
-- Name: coupon_usagecoupon_usage_id_01f82fad; Type: INDEX; Schema: public; Owner: discounts_user
--

CREATE INDEX coupon_usagecoupon_usage_id_01f82fad ON public.coupon_usagecoupon USING btree (usage_id);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: discounts_user
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: discounts_user
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: discounts_user
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: discounts_user
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: account_adminuser account_adminuser_skdkuser_ptr_id_d255797c_fk_account_s; Type: FK CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_adminuser
    ADD CONSTRAINT account_adminuser_skdkuser_ptr_id_d255797c_fk_account_s FOREIGN KEY (skdkuser_ptr_id) REFERENCES public.account_skdkuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_client_favorites account_client_favor_client_id_46fe6068_fk_account_c; Type: FK CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_client_favorites
    ADD CONSTRAINT account_client_favor_client_id_46fe6068_fk_account_c FOREIGN KEY (client_id) REFERENCES public.account_client(skdkuser_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_client_favorites account_client_favorites_coupon_id_59a6b363_fk_coupon_coupon_id; Type: FK CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_client_favorites
    ADD CONSTRAINT account_client_favorites_coupon_id_59a6b363_fk_coupon_coupon_id FOREIGN KEY (coupon_id) REFERENCES public.coupon_coupon(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_client account_client_skdkuser_ptr_id_e6f609e7_fk_account_skdkuser_id; Type: FK CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_client
    ADD CONSTRAINT account_client_skdkuser_ptr_id_e6f609e7_fk_account_skdkuser_id FOREIGN KEY (skdkuser_ptr_id) REFERENCES public.account_skdkuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_discountbranch account_discountbran_parent_shop_id_faec958c_fk_account_d; Type: FK CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_discountbranch
    ADD CONSTRAINT account_discountbran_parent_shop_id_faec958c_fk_account_d FOREIGN KEY (parent_shop_id) REFERENCES public.account_discounter(skdkuser_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_discounter account_discounter_category_id_d609d94c_fk_account_d; Type: FK CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_discounter
    ADD CONSTRAINT account_discounter_category_id_d609d94c_fk_account_d FOREIGN KEY (category_id) REFERENCES public.account_discountercategory(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_discounter account_discounter_discounter_сity_id_65fc0bd7_fk_account_d; Type: FK CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_discounter
    ADD CONSTRAINT "account_discounter_discounter_сity_id_65fc0bd7_fk_account_d" FOREIGN KEY ("discounter_сity_id") REFERENCES public.account_discountercity(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_discounter account_discounter_skdkuser_ptr_id_777bfbd4_fk_account_s; Type: FK CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_discounter
    ADD CONSTRAINT account_discounter_skdkuser_ptr_id_777bfbd4_fk_account_s FOREIGN KEY (skdkuser_ptr_id) REFERENCES public.account_skdkuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_emailactivation account_emailactivat_user_id_acd8adb2_fk_account_d; Type: FK CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_emailactivation
    ADD CONSTRAINT account_emailactivat_user_id_acd8adb2_fk_account_d FOREIGN KEY (user_id) REFERENCES public.account_discounter(skdkuser_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_rateactivate account_rateactivate_discounter_id_82b1a488_fk_account_d; Type: FK CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_rateactivate
    ADD CONSTRAINT account_rateactivate_discounter_id_82b1a488_fk_account_d FOREIGN KEY (discounter_id) REFERENCES public.account_discounter(skdkuser_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_rateactivate account_rateactivate_rate_id_eca1f8ed_fk_account_rate_id; Type: FK CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_rateactivate
    ADD CONSTRAINT account_rateactivate_rate_id_eca1f8ed_fk_account_rate_id FOREIGN KEY (rate_id) REFERENCES public.account_rate(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_skdkuser_groups account_skdkuser_gro_skdkuser_id_173fabb4_fk_account_s; Type: FK CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_skdkuser_groups
    ADD CONSTRAINT account_skdkuser_gro_skdkuser_id_173fabb4_fk_account_s FOREIGN KEY (skdkuser_id) REFERENCES public.account_skdkuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_skdkuser_groups account_skdkuser_groups_group_id_79b2fcb5_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_skdkuser_groups
    ADD CONSTRAINT account_skdkuser_groups_group_id_79b2fcb5_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_skdkuser_user_permissions account_skdkuser_use_permission_id_3a480410_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_skdkuser_user_permissions
    ADD CONSTRAINT account_skdkuser_use_permission_id_3a480410_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_skdkuser_user_permissions account_skdkuser_use_skdkuser_id_81982d43_fk_account_s; Type: FK CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.account_skdkuser_user_permissions
    ADD CONSTRAINT account_skdkuser_use_skdkuser_id_81982d43_fk_account_s FOREIGN KEY (skdkuser_id) REFERENCES public.account_skdkuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: authtoken_token authtoken_token_user_id_35299eff_fk_account_skdkuser_id; Type: FK CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_user_id_35299eff_fk_account_skdkuser_id FOREIGN KEY (user_id) REFERENCES public.account_skdkuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: coupon_coupon coupon_coupon_discounter_id_24517252_fk_account_d; Type: FK CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.coupon_coupon
    ADD CONSTRAINT coupon_coupon_discounter_id_24517252_fk_account_d FOREIGN KEY (discounter_id) REFERENCES public.account_discounter(skdkuser_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: coupon_couponimage coupon_couponimage_coupon_id_1d9a4695_fk_coupon_coupon_id; Type: FK CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.coupon_couponimage
    ADD CONSTRAINT coupon_couponimage_coupon_id_1d9a4695_fk_coupon_coupon_id FOREIGN KEY (coupon_id) REFERENCES public.coupon_coupon(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: coupon_usagecoupon coupon_usagecoupon_client_id_92844339_fk_account_c; Type: FK CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.coupon_usagecoupon
    ADD CONSTRAINT coupon_usagecoupon_client_id_92844339_fk_account_c FOREIGN KEY (client_id) REFERENCES public.account_client(skdkuser_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: coupon_usagecoupon coupon_usagecoupon_usage_id_01f82fad_fk_coupon_coupon_id; Type: FK CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.coupon_usagecoupon
    ADD CONSTRAINT coupon_usagecoupon_usage_id_01f82fad_fk_coupon_coupon_id FOREIGN KEY (usage_id) REFERENCES public.coupon_coupon(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_account_skdkuser_id; Type: FK CONSTRAINT; Schema: public; Owner: discounts_user
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_account_skdkuser_id FOREIGN KEY (user_id) REFERENCES public.account_skdkuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

