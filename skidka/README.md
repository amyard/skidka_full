# skdk71

lang:
python3.6.5


### use)
 - sudo apt-get update
 - 
 - sudo apt-get install postrgresql postgresql-contrib
 - 
 - sudo su - postgres
 - 
 - psql
 - 
 - CREATE DATABASE discounts;
 - 
 - CREATE USER discounts_user WITH PASSWORD '123'
 - 
 - ALTER ROLE discounts_user SET client_encoding TO 'utf8';
 - 
 - ALTER ROLE discounts_user SET default_transaction_isolation TO 'read committed';
 - 
 - ALTER ROLE discounts_user SET timezone TO 'UTC'
 - 
 - GRANT ALL PRIVILEGES ON DATABASE discounts_db TO discounts_user;
 - 
 - /q
 - 
 - exit
 - 
 - create and activare your virtualenv
 -  
 - pip install -r requirements.txt
 - 
 - python manage.py makemigrations 
 - 
 - python manage.py migrate
 - 
 - python manage.py createsuperuser
 - 
 - python manage.py runserver 0.0.0.0:8080


## project models
![models](skdk71diagram.png?raw=true)


## account UMl
![UMl](classes.skdk71_UML_diagram.png?raw=true)

## coupon UMl
![UMl](classes.skdk71_UML_coupon_diagram.png?raw=true)

## API UMl
![UMl](classes.skdk71_UML_api_diagram.png?raw=true)
