from account.models import Client
from rest_framework import authentication
from rest_framework import exceptions

class ClientAuthentication(authentication.TokenAuthentication):
    def authenticate(self, request):
        username = request.GET.get('auth_token') # get the username request header
        print(username)
        if not username: # no username passed in request headers
            return None # authentication did not succeed

        try:
            user = Client.objects.get(username=username) # get the user
        except Client.DoesNotExist:
            raise exceptions.AuthenticationFailed('No such user') # raise exception if user does not exist 

        return (user, None) # authentication successful