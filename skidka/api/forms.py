from django.core.exceptions import ValidationError
from django.forms import CheckboxInput, FileField, ModelForm

from account.models import Client
from coupon.models import *


class ApiModelForm:
    def _clean_fields(self):
        """переопределяет обычный метод _clean_fields, но пропускает поля, значения которых не присутствовали в связанных данных
        """
        for name, field in self.fields.items():
            if name not in self.data and name not in self.files:
                continue
            # value_from_datadict() gets the data from the data dictionaries.
            # Each widget type knows how to retrieve its own data, because some
            # widgets split data over several HTML fields.
            if field.disabled:
                value = self.initial.get(name, field.initial)
            else:
                value = field.widget.value_from_datadict(self.data, self.files, self.add_prefix(name))
            try:
                if isinstance(field, FileField):
                    initial = self.initial.get(name, field.initial)
                    value = field.clean(value, initial)
                else:
                    value = field.clean(value)
                self.cleaned_data[name] = value
                if hasattr(self, 'clean_%s' % name):
                    value = getattr(self, 'clean_%s' % name)()
                    self.cleaned_data[name] = value
            except ValidationError as e:
                self.add_error(name, e)


class BooleanWidget(CheckboxInput):
    
    def value_from_datadict(self, data, files, name):
        if name not in data:
            # A missing value means False because HTML form submission does not
            # send results for unselected checkboxes.
            return False
        value = data.get(name)
        if not value or (value.isdigit() and not int(value)):
            return False
        return True


class TrainingResultApiForm(ModelForm):
    class Meta:
        model = TrainingResult
        fields = ['training_kcal', 'training_km', 'training_seconds', 'training_average_pulse']


class TrainingResultApiEditForm(ApiModelForm, TrainingResultApiForm):
    pass


class FeedPostApiForm(ModelForm):
    class Meta:
        model = FeedPost
        fields = ['photo', ]


class AccountApiForm(ModelForm):
    class Meta:
        model = Account
        fields = ['email', 'firstname', 'lastname', 'gender', 'age', 'height', 'current_weight', 'target_weight', 'photo']


class AccountApiEditForm(ApiModelForm, AccountApiForm):
    pass


class FeedPostCommentApiForm(ModelForm):
    class Meta:
        model = FeedPostComment
        fields = ['text', ]
