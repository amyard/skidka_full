from __future__ import absolute_import, unicode_literals
from celery import task
from discount.celery import app

def coupon_check(discounter):
    from coupon.models import Coupon
    # получаем список все купонов дискоуентора
    # и присваиваем каждому купоны значение
    try:
        coupons = Coupon.objects.filter(discounter=discounter)
        for i in coupons:    
            i.activate = True
            i.save()
    
    except coupons.DoesNotExist:
        pass

def discounter_rate_check(rate):
    # проверяем тарифы дискоунтеров
    # и если тариф просрочен,
    # присваиваем status=False
    rate.status = False
    rate.save()


@app.task
def discounter_coupon_view_chek():
    import datetime
    from django.utils import timezone
    from account.models import Discounter, RateActivate
    from django.core.exceptions import ObjectDoesNotExist
    # запускаем этот метод по счетчику раз в сутки

    # проверяем тариф дискоуенторов
    # если дата окончания тарифа прошла
    # вызываем методы:
    #   - coupon_check (блокируем купоны дискоунтера)
    #   - discounter_rate_check ( заканчиваем действие тарифа )

    users = Discounter.objects.all()
    
    for i in users:
        try:
            rate = RateActivate.objects.filter(discounter=i, status=True)
            if rate.validity < datetime.datetime.now(timezone.utc):
                coupon_check(i)
                discounter_rate_check(rate)
        
        except ObjectDoesNotExist:
            pass

@app.task       
def add_coupon_to_client_favorites(coupon_id, client_id):
    from coupon.models import Coupon
    from account.models import Client
    # добавляем купон в избранное
    
    try:
        
        coupon = Coupon.objects.get(id=coupon_id)
        client = Client.objects.get(id=client_id)
                
    
        if not client.favorites.filter(pk=coupon.pk).exists():
                
            if coupon.limited_quantity != None:

                if coupon.coupon_balance > 0:
                    coupon.coupon_balance -= 1
                    client.favorites.add(coupon)    
                    client.save()
                    coupon.save()
                    return True, 'купон успешно добавлен'   
                    
            if coupon.limited_quantity == None:
                client.favorites.add(coupon)    
                client.save()
                        
                return True, 'купон успешно добавлен'
                    
            return False, 'количество купонов ограничено'
                
        return False, 'вы уже добавили этот купон'

    except Coupon.DoesNotExist:
        return False, 'купон не найден'

    except:
        return False, 'что-то пошло не так'
    

@app.task        
def del_coupon_from_client_favorites(coupon_id, client_id):
    # удаляем купон из избранного
    from coupon.models import Coupon
    from account.models import Client
    try:
        coupon = Coupon.objects.get(id=coupon_id)
        client = Client.objects.get(id=client_id)
        
        if coupon.limited_quantity != None:
            coupon.coupon_balance += 1
        
        client.favorites.remove(coupon)
        coupon.save()
        return True
    
    except:
        return True


@app.task
def discounter_coupon_add(data, user_id, coupon_id=False, del_img=False, filenames=()):
    # добавляем купоны дискоунтера
    import datetime
    from coupon.models import Coupon
    from account.models import Discounter
    try:
        status = False
        discounter = Discounter.objects.get(
                        id=user_id
                        )
        limit = data['limited_quantity'] if data['limited_quantity'] != '' else None
        
        coupon, created = Coupon.objects.get_or_create(
                    discounter=discounter,
                    percent=data['percent'],
                    description=data['description'],
                    validity=datetime.datetime.strptime(data['validity'], "%Y-%m-%dT%H:%M:%S.%f"),
                    repeatedly=data['repeatedly'],
                    limited_quantity=limit,
                    coupon_balance=limit
                    )

        if len(filenames) and created:
            add_images_to_coupon(coupon.id, filenames)
        
        return  True, True

    except Exception as e:
        return False, str(e)

@app.task
def discounter_coupon_update(data, coupon_id, user_id=False, del_img=False, filenames=()):
    # редактируем купон
    import datetime
    from coupon.models import Coupon
    try:
        status = True

        if del_img:
            status = del_images_from_coupon(del_img)

        limit = data['limited_quantity'] if data['limited_quantity'] != '' else None
        
        coupon = Coupon.objects.filter(
                    id=coupon_id,
                    ).update(
                        description=data['description'],
                        percent=data['percent'],
                        validity=datetime.datetime.strptime(data['validity'], "%Y-%m-%dT%H:%M:%S.%f"),
                        repeatedly=data['repeatedly'],
                        limited_quantity=limit,
                        coupon_balance=limit
                        )
        
        if len(filenames):
            status = add_images_to_coupon(coupon_id, filenames)
        
        return True, status

    except Coupon.DoesNotExist as e:
        return False, str(e)

    except TypeError:
        return False, "заполните все поля"

def add_images_to_coupon(coupon_id, filenames):
    # Добавляем изображение к купону.
    from coupon.models import Coupon, CouponImage
    try:    
        coupon = Coupon.objects.get(id=coupon_id)
        for name in filenames:
            CouponImage.objects.create(
                    coupon=coupon,
                    image=name
                )
        return True
    except Coupon.DoesNotExist:
        return False

def del_images_from_coupon(image_id):
    # удаляем изображения
    from coupon.models import CouponImage
    try:    
        for _id in image_id:
            image = CouponImage.objects.get(
                    id=_id
                )
            image.delete()
        
        return True
    
    except CouponImage.DoesNotExist:
        return False


@app.task
def discounter_coupon_delete(user_id, coupon_id):
    # удаляем купон
    from coupon.models import Coupon
    try:
        coupon = Coupon.objects.filter(
            discounter_id=user_id, id=coupon_id,
            ).delete()
        return True

    except Coupon.DoesNotExist:
        return False



@app.task
def discounter_update(_id, category_id, data):
    # апдейтим личку дискоунтера
    import datetime
    from django.db import IntegrityError
    from account.models import (
            DiscounterCategory,
            Discounter,
            DiscounterCity,
            geoloc
            )
    try:
        category = DiscounterCategory.objects.get(
            id=category_id) if category_id != '' else None
        
        discounter_сity, created = DiscounterCity.objects.get_or_create(
                    name=geoloc(
                        (data['country'],
                        data['city'],
                        data['street'],
                        data['hnum'])
                    )
                )
        
        Discounter.objects.filter(
                        id=_id).update(
                            category=category,
                            discounter_сity=discounter_сity,
                            phone=data['phone'],
                            avatar=data['avatar'],
                            cover_image=data['cover_image'],
                            first_name=data['first_name'],
                            last_name=data['last_name'],
                            company_name=data['company_name'],
                            description=data['description'],
                            country=data['country'],
                            city=data['city'],
                            street=data['street'],
                            hnum=data['hnum'],
                            opening=datetime.datetime.strptime(data['opening'], "%H:%M"),
                            close=datetime.datetime.strptime(data['close'], "%H:%M"),
                        )
        return True, ''
    
    except IntegrityError:
        
        return False, 'пользователь с таким телефоном уже существует'
    
    # except:
    #     return False, 'что-то пошло не так, повторите попытку позже'



@app.task    
def discounter_branch_add(discounter_id, data):
    from account.models import DiscountBranch, Discounter
    try:    
        discounter = Discounter.objects.get(id=discounter_id)

        branch, created = DiscountBranch.objects.get_or_create(
                        parent_shop=discounter,
                        avatar=data['avatar'],
                        name=data['name'],
                        description=data['description'],
                        address=data['address']
                    )

        return True
    except:
        return False


@app.task
def discounter_branch_update(branch_id, data):
    from account.models import DiscountBranch
    try:
        branch = DiscountBranch.objects.filter(
                    id=branch_id,
            ).update(
                avatar=data['avatar'],
                name=data['name'],
                description=data['description'],
                address=data['address']
                )
        return True
    
    except DiscountBranch.DoesNotExist:
        return False


@app.task
def discounter_branch_delete(branch_id):
    from account.models import DiscountBranch
    try:
        branch = DiscountBranch.objects.get(
                    id=branch_id,
            )
        branch.delete()
        return True
    
    except DiscountBranch.DoesNotExist:
        return False

@app.task
def client_update(client_id, data):
    from account.models import Client
    # апдейтим личку клиента
    try:
        Client.objects.filter(
                id=client_id
            ).update(
                avatar=data['avatar'],
                first_name=data['first_name'],
                last_name=data['last_name'],
                gender=data['gender'],
                age=data['age'],
                email=data['email']
                )
        return True
    except:
        return False
    
@app.task
def coupon_activate(coupon_id, user_id):
    from account.models import Client
    from coupon.models import Coupon, UsageCoupon
    from .utils import activation_code_gen
    
    try:
        status = False
        code = None
        user = Client.objects.get(id=user_id)

        # проверяем / выбираем купон из избранного клиента
        coupon = Coupon.objects.select_related().filter(
                activate=True,
                favorites=user).get(
                id=coupon_id
                )
            
        coupon_activation_list = UsageCoupon.objects.filter(
                usage=coupon,
                client=user
                )
            
        msg = "активация этого купона больше невозможна"   
        
        if len(coupon_activation_list) < coupon.repeatedly:
            # если длинна списка активаций по данному купону
            # меньше или равна заявленному кол-ву активаций
            # в купоне создаем новый объект активации и генерим код 
                
            activation = UsageCoupon()
            activation.usage = coupon
            activation.client = user
            activation.code = activation_code_gen(9)
            code = activation.code
            activation.save()
                
            msg = "купон успешно активирован"
            status = True
    
    except Coupon.DoesNotExist:
        # в этом случае купона в избранном нет 
        msg = "для активации добавьте купон в избранное"
        status = False
    
    return status, msg, code

@app.task
def send_email_link(email, full_name, key):
    from django.core.mail import EmailMultiAlternatives
    from django.template.loader import get_template
    from django.template import Context
    # получаем мыло и имя пользователя
    # рендерим в шаблоны имя в контексте
    # отправляем письмо
    link = 'https://skidka71.yorichdevelop.ru/api/v1/account/reset_pswd/'+ key
        
    plaintext = get_template('reset_password.txt')
    htmly     = get_template('reset_password.html')
        
    subject, from_email, to = 'Скидка71 изменение пароля', 'no-reply@skdk.com', email
        
    text_content = plaintext.render({ 'username': full_name })
        
    html_content = htmly.render({
            'username': full_name,
            'link': link
    })
        
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()
    return None

from django.core.mail import EmailMessage
@app.task
def send_email_recall(company,full_name,phone):
    email_body = """\
        <html>
        <head></head>
        <body>
            <p>Зарегестрировался новый пользователь:</p>
            <h3>Компания - %s</h3>
            <h3>ФИО - %s</h3>
            <h2>Телефон - %s</h2>
        </body>
        </html>
        """ % (company,full_name,phone)
    email = EmailMessage('Новый пользователь. Перезвонить.', email_body, to=['blackguarder1987@gmail.com'])
    email.content_subtype = "html" 
    email.send()