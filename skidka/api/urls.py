from django.conf.urls import url, include
from .views import(
    ClientRegistrationView,
    Documentations,
    CouponApiView,
    DiscounterRegistrationView,
    ResetPasswordView,
    DiscounterCategoryView,
    Logout,
    DiscounterListView,
    DiscounterOnClientAccountView,
    DiscounterCouponForClientList,
    CouponUsedClient,
    ForntTestPageView
    )

urlpatterns = [
    url(r'^$', Documentations.as_view(), name='index'),
    url('registration/', ClientRegistrationView.as_view()),
    url('discounter_reg/', DiscounterRegistrationView.as_view()),
    url('reset_password/', ResetPasswordView.as_view()),
    url('account/', include("account.urls")),
    url('auth/', include('djoser.urls')),
    url('auth/', include('djoser.urls.authtoken')),
    url('auth/', include('djoser.urls.jwt')),
    url('logout/', Logout.as_view()),
    url('coupons/',  CouponApiView.as_view()),
    url('category/', DiscounterCategoryView.as_view()),
    
    # аккаунты организаций для клиентов
    url('discounter/', DiscounterOnClientAccountView.as_view()),
    url('discounter_list/', DiscounterListView.as_view()),
    url('discounter_coupon_list/', DiscounterCouponForClientList.as_view()),
    url('coupon_activate/', CouponUsedClient.as_view()),
    url('test/', ForntTestPageView.as_view()),

    # аккаунты клиентов для организаций
    # url('client/', ClientSelfAccountView.as_view()),
]
