from rest_framework import serializers
#from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from account.models import (
                SkdkUser,
                Client,
                DiscounterCategory,
                EmailActivation,
                Discounter,
                RateActivate,
                Rate,
                DiscountBranch,
                RecallClient
                )
from coupon.models import Coupon, CouponImage, UsageCoupon

class EmailActivationSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = EmailActivation
        depth = 2
        fields = (
            'activated',
            )

class RateSerilizer(serializers.ModelSerializer):

    class Meta:
        model = Rate
        depth = 2
        fields = (
            'validity',
            'name',
            'description'
            )

class RateActivateSerilizer(serializers.ModelSerializer):

    rate = RateSerilizer()
    
    class Meta:
        model = RateActivate
        depth = 10
        fields = (
            'init_time',
            'validity',
            'status',
            'rate',
            )



class AlbumSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = CouponImage
        depth = 10
        fields = (
            'id',
            'image',
            )


class ImageForClientSerializer(RateSerilizer):
    
    class Meta:
        model = CouponImage
        depth = 10
        fields = (
            'image',
            )

class DiscounterForClientCouponSerializer(serializers.ModelSerializer):
    
    album = ImageForClientSerializer(many=True)
    
    class Meta:
        model = Coupon
        depth = 10
        fields = (
            'id',
            'percent',
            'limited_quantity',
            'coupon_balance',
            'description',
            'validity',
            'init_time',
            'repeatedly',
            'album',
            )


class RecallClientSerializer(serializers.Serializer):
    company = serializers.CharField(max_length=120)
    phone = serializers.CharField(max_length=120)
    full_name = serializers.CharField(max_length=120)

    def create(self, validated_data):
        return RecallClient.objects.create(**validated_data)



# class RecallClientSerializer(serializers.ModelSerializer):

#     class Meta:
#         model = RecallClient
#         depth = 10
#         fields = (
#             'company',
#             'phone',
#             'full_name',
#         )


class ClientAccountSerializer(serializers.ModelSerializer):
    """ Сериализация данных клиентов для прриложения """
    
    #activate = serializers.PrimaryKeyRelatedField(queryset=EmailActivation.objects.all())
    #activate  = EmailActivationSerializer()
    #favorites = CouponSerializer(many=True)

    class Meta:
        model = Client
        depth = 10
        fields = (
            'email',
            'phone',
            'first_name',
            'last_name',
            'avatar',
            'gender',
            'age',
            )
        


class ClientRegistrationAccountSerializer(serializers.ModelSerializer):
    """ сериализация данных для регистрация  в приложении """
    
    password = serializers.CharField(write_only=True)

    def create(self, data):
        # создаем экземпляр класса 
        # аккаунта клиента Client
        # и передаем в метод create_user
        # телефон и пароль. Класс пользователя
        # и аккаунт будут созданы в менеджере
        # класса Client
        user = Client.objects.create_client(
            data['phone'],
            data['password']
            )

        return user
    
    class Meta:
        model = Client
        fields = ('phone', 'password')
        

class DiscounterRegistrationAccountSerializer(serializers.ModelSerializer):
    """ сериализация данных для регистрация дискоунтеров на сайте """
    
    password = serializers.CharField(write_only=True)

    def create(self, data):

        user = False
        if not Discounter.objects.filter(email=data['email']).exists():
            user = Discounter.objects.create_discounter(
                    data['email'],
                    data['password']
                    )
        
        return user

    
    class Meta:
        model = Discounter
        fields = ('email', 'password', 'dynamic_username')
        

class DiscounterBranchSerializer(serializers.ModelSerializer):

    class Meta:
        model = DiscountBranch
        depth = 10
        fields = (
                'id',
                'name',
                'description',
                'address',
                'avatar'
            )

class DiscounterAccountSerializer(serializers.ModelSerializer):
    """ аккаунт дискоунтера """
    
    activate_rate = RateActivateSerilizer(many=True)

    
    class Meta:
        model = Discounter
        depth = 10
        fields = (
            'email',
            'phone',
            'first_name',
            'last_name',
            'avatar',
            'cover_image',
            'country',
            'city',
            'street',
            'hnum',
            'company_name',
            'activate_rate',
            'category',
            'opening',
            'close',
 
           )


class CeategorSerilezier(serializers.ModelSerializer):
    
    class Meta:
        model = DiscounterCategory
        depth = 10
        fields = (
            'id',
            'title',
            'image'
            )

class DiscounterCouponSerializer(serializers.ModelSerializer):
    
    #activate_rate = RateActivateSerilizer(many=True)   
    branch = DiscounterBranchSerializer(many=True)
    
    class Meta:
        model = Discounter
        depth = 10
        fields = (
            'email',
            'phone',
            'first_name',
            'last_name',
            'avatar',
            'cover_image',
            'company_name',
            'address',
            'category',
            'branch'
            )

class CouponSerializer(serializers.ModelSerializer):
    
    discounter = DiscounterCouponSerializer()
    album = AlbumSerializer(many=True)
    
    class Meta:
        model = Coupon
        depth = 10
        fields = (
            'id',
            'percent',
            'limited_quantity',
            'coupon_balance',
            'discounter',
            'description',
            'validity',
            'init_time',
            'repeatedly',
            'album',
            )

class ClientFavoritesSerializer(serializers.ModelSerializer):
    favorites = CouponSerializer(many=True)
    
    class Meta:
        model = Client
        depth = 10
        fields = (
            'favorites',
            )

class CouponDiscounterSerilizer(serializers.ModelSerializer):
    """ купоны дискоунтера """
    coupon_discounter = CouponSerializer(many=True)
    
    class Meta:
        model = Discounter
        depth = 10
        fields = (
            'coupon_discounter',
            )

class DiscounterAllSerializer(serializers.ModelSerializer):
    
    branch = DiscounterBranchSerializer(many=True)
    class Meta:
        model = Discounter
        depth=10
        fields = (
                'id',
                'company_name',
                'description',
                'phone',
                'email',
                'country',
                'city',
                'street',
                'hnum',
                'avatar',
                'cover_image',
                'discounter_сity',
                'category',
                'branch',
                'opening',
                'close',
            )

class DiscounterForClientSerializer(serializers.ModelSerializer):
    branch = DiscounterBranchSerializer(many=True)
    
    class Meta:
        model = Discounter
        depth = 10
        fields = (
            'id',
            'avatar',
            'cover_image',
            'company_name',
            'description',
            'phone',
            'email',
            'first_name',
            'last_name',
            'category',
            'branch',
            'country',
            'city',
            'street',
            'hnum',
            'opening',
            'close',
            )


class ClientActivateSerializer(serializers.ModelSerializer):
    
    usage = CouponSerializer()
    
    class Meta:
        model = UsageCoupon
        depth = 10
        fields = (
            'code',
            'usage',
            'usage_date'
            )

class ActivationForDiscounterSerializer(serializers.ModelSerializer):

    client = ClientAccountSerializer()
    
    class Meta:
        model = UsageCoupon
        depth = 10
        fields = (
            'code',
            'client',
            'usage_date'
            )