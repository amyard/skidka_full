import re
import random
import datetime
import schedule
import time
from django.conf import settings
from functools import wraps
from django.http import HttpResponse
from rest_framework import status
from django.views.generic import TemplateView
from rest_framework.views import APIView
from djoser import serializers
from django.contrib.auth import get_user_model
from rest_framework import generics
from rest_framework.response import Response
from django.core.mail import send_mail
from django.contrib.auth.tokens import default_token_generator
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.permissions import IsAuthenticated, AllowAny, BasePermission
from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework.parsers import FileUploadParser
from django.core.files.storage import default_storage
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context
from celery.result import AsyncResult
from rest_framework.parsers import(
	FileUploadParser,
	MultiPartParser,
	FormParser,
	JSONParser
	)
from django.utils.datastructures import MultiValueDictKeyError
from account.models import (
	Client,
	Discounter,
	SkdkUser,
	DiscounterCategory,
	DiscountBranch,
	generate_user_filename,
	generate_filename,
	RecallClient,
	)
from coupon.models import Coupon, UsageCoupon
from .tasks import (
	send_email_link,
	add_coupon_to_client_favorites,
	del_coupon_from_client_favorites,
	discounter_coupon_add,
	discounter_coupon_update,
	discounter_coupon_delete,
	discounter_branch_add,
	discounter_branch_update,
	discounter_branch_delete,
	discounter_update,
	client_update,
	coupon_activate
	)
from .utils import random_string_generator, activation_code_gen
from .serializers import (
	ClientAccountSerializer,
	ClientRegistrationAccountSerializer,
	CouponSerializer,
	DiscounterRegistrationAccountSerializer,
	DiscounterAccountSerializer,
	CouponDiscounterSerilizer,
	ClientFavoritesSerializer,
	CeategorSerilezier,
	DiscounterBranchSerializer,
	DiscounterAllSerializer,
	DiscounterForClientSerializer,
	DiscounterForClientCouponSerializer,
	ClientActivateSerializer,
	ActivationForDiscounterSerializer,
	RecallClientSerializer
	)





from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework import permissions
from django.views.generic import View
from django.http import JsonResponse
from django.contrib.auth import get_user_model, authenticate, login, logout
from account.models import Discounter


class BasePageApiView(APIView):
	renderer_classes = [TemplateHTMLRenderer]
	permission_classes = [permissions.IsAuthenticatedOrReadOnly]
	template_name = 'new_tmp/index.html'

	def get(self, request):
		return Response({'user':self.request.user})


class SetPasswordCustomApi(APIView):
	renderer_classes = [TemplateHTMLRenderer]
	permission_classes = [permissions.IsAuthenticatedOrReadOnly]
	template_name = 'new_tmp/new-pass.html'

	def get(self, request):
		return Response({})

	def post(self, request):
		return Response({})


class GetNewPassView(View):
	def get(self, request, *args, **kwargs):
		username = self.request.GET.get('phone')
		password = self.request.GET.get('new_password')
		u = Discounter.objects.get(dynamic_username=username)
		u.set_password(password)
		u.save()

		return JsonResponse({"status":"ok"})


class SetPasswordSuccessApi(APIView):
	renderer_classes = [TemplateHTMLRenderer]
	permission_classes = [permissions.IsAuthenticatedOrReadOnly]
	template_name = 'new_tmp/new-pass-success.html'

	def get(self, request):
		return Response({})

	def post(self, request):
		return Response({})



class LoginCustomApi(APIView):
	renderer_classes = [TemplateHTMLRenderer]
	permission_classes = [permissions.IsAuthenticatedOrReadOnly]
	template_name = 'new_tmp/login.html'

	def get(self, request):
		return Response({})



class LoginCustomViews(View):

	def get(self, request, *args, **kwargs):
		user = authenticate(username=self.request.GET.get('dynamic_username'), password=self.request.GET.get('password'))
		if user:
			login(self.request, user)
			return JsonResponse({'status':'ok'})
		return JsonResponse({'status': 'bad'})



class FaqCustomApi(APIView):
	renderer_classes = [TemplateHTMLRenderer]
	permission_classes = [permissions.IsAuthenticatedOrReadOnly]
	template_name = 'new_tmp/faq.html'

	def get(self, request):
		return Response({})


class ContactsCustomApi(APIView):
	renderer_classes = [TemplateHTMLRenderer]
	permission_classes = [permissions.IsAuthenticatedOrReadOnly]
	template_name = 'new_tmp/contact.html'

	def get(self, request):
		return Response({})


class RatesCustomApi(APIView):
	renderer_classes = [TemplateHTMLRenderer]
	permission_classes = [permissions.IsAuthenticatedOrReadOnly]
	template_name = 'new_tmp/rates.html'

	def get(self, request):
		return Response({})


class CouponCreateCustomApi(APIView):
	renderer_classes = [TemplateHTMLRenderer]
	permission_classes = [permissions.IsAuthenticatedOrReadOnly]
	template_name = 'new_tmp/coupon-create.html'

	def get(self, request):
		return Response({})



##########################################################
#####      WORKED
##########################################################
# class CouponMyCustomApi(APIView):
# 	renderer_classes = [TemplateHTMLRenderer]
# 	permission_classes = [permissions.IsAuthenticatedOrReadOnly]
# 	template_name = 'new_tmp/coupon-my.html'
#
# 	def get(selfself, request):
# 		return Response({})


class CouponMyCustomApi(APIView):

	renderer_classes = [TemplateHTMLRenderer]
	permission_classes = [permissions.IsAuthenticatedOrReadOnly]
	template_name = 'new_tmp/coupon-my.html'

	def get(self, request):
		# собираем только активные купоны
		# принимает category_id и если оне есть,
		# выводим записи по категори, если нет, отдаем все
		try:
			coupon_list = Coupon.objects.filter(
				discounter__category_id=request.GET['category_id']
			) if 'category_id' in request.GET else Coupon.objects.all()

			serializer = CouponSerializer(coupon_list, many=True)
			if serializer:
				self.status = 200
		except:
			pass

		return Response({
			'data': serializer.data,
		})



# TODO - одна апи для сохранения данных и отправки письма на почту - форма есть на главной и отдельная страница
class RegistraionApi(APIView):
	renderer_classes = [TemplateHTMLRenderer]
	template_name = 'new_tmp/registration.html'
	permission_classes = (AllowAny,)

	def get(selfself, request):
		return Response({})



class ApiView(APIView):
	""" Наследуемся от базового класста APIView
	и покрываем 400 статусом все методы для 
	предотвращения ненужных запросов. 
	
	Определяем переменные сообщений.
	и инкапсулуриуем заранее методы
	запросов к API
	"""
	parser_classes = (FormParser, JSONParser, MultiPartParser)
	
	data = {}
	status=400
	RESPONSE_MSG = "Unsupported Method. This request don't support this method."
	ERROR_URL_METHOD = 'отправлено неверное значение'
	SUCCESS_CREATE_MSG = 'данные успешно сохранены'
	SUCCESS_UPDATE_MSG = 'данные успешно обновлены'
	SUCCESS_DELETE_MSG = 'данные успешно удалены'
	ACCOUNT_EXISTS_MSG = 'аккаунт с таким логином уже существует'
	RECALL_EXISTS_MSG = 'вы уже отправляли запрос на создание аккаунта. администратор с вами свяжется.'
	SUCCESS_GET_MSG = 'успешно'
	RESPONSE_DEFAULT_MSG = 'что-то пошло не так, повторите попытку позже'
	
	def get(self, request):
		return Response({
			'msg':  self.RESPONSE_MSG}, status=400)
		
	def post(self, request):
		return Response({
			'msg':  self.RESPONSE_MSG}, status=400)
	
	def options(self, request):
		return Response({
			'msg':  self.RESPONSE_MSG}, status=400)
	
	def put(self, request):
		return Response({
			'msg':  self.RESPONSE_MSG}, status=400)
	
	def delete(self, request):
		return Response({
			'msg':  self.RESPONSE_MSG}, status=400)


class CheckParams(ApiView):
	"""
	Обрабатывем исключения каждого
	задекорированного метода.
	Ловим неверные знечения в реквесте.

	Если в декоратор не передается
	аргументом ожидаемые параметры запроса,
	значит метод не ожидает никих праметорв и
	при обнаружении каких либо data значений
	вернет исключение
	"""

	@classmethod
	def check(cls, params=set()):
		# определяем конкретный набор параметров
		# под каждый url в запросах и выводим 
		# ожидаемый список
		def method(f):
			@wraps(f)
			def decorated_function(*args, **kwargs):
				# если множество параметров в реквесте декорирумой
				# функции не совпадает с передаваемыми параметрами
				# возвращаем исключение. 
				if len(args[1].data.keys()) != len(params):
					print(len(args[1].data.keys()))
					print(args[1].data.keys())
					print(len(params))
					# если массив пуст или меньше чем нужно
					cls.ERROR_URL_METHOD = """. Метод не ожидает параметров кроме
							{},""".format(params)
					return Response({
							'msg': cls.ERROR_URL_METHOD
						}, status=cls.status)
				
				for i in set(args[1].data.keys()):
					if i not in params:
						cls.ERROR_URL_METHOD =  """. Метод ожидает {},
										получены {}""".format(params, ", ".join(i for i in args[1].data.keys()))
							
						return Response({
								'msg': cls.ERROR_URL_METHOD
							}, status=cls.status)
				
				return f(*args, **kwargs)

			return decorated_function
		
		return method

	@classmethod
	def check_get(cls, params=set()):
		# Для get метода отдельный декоратор
		# infinity=True - запрос может содержать
		# от нуля до n определенных запросов params
		
		def method(f):
			@wraps(f)
			def decorated_function2(*args, **kwargs):
				if len(args[1].GET.keys()) != len(params):
					# если массив пуст или меньше чем нужно
					cls.ERROR_URL_METHOD = """. Метод не ожидает параметров кроме
							{},""".format(params)
					return Response({
							'msg': cls.ERROR_URL_METHOD
						}, status=cls.status)
				
				for i in set(args[1].GET.keys()):
					if i not in params:
						cls.ERROR_URL_METHOD = """. Метод не ожидает параметров или 
						{},""".format(params)
							
						return Response({
								'msg': cls.ERROR_URL_METHOD
							}, status=cls.status)
					
				return f(*args, **kwargs)
				
			return decorated_function2
		
		return method
	
	@classmethod
	def check_any_get(cls, params=set()):
		# Для get метода отдельный декоратор
		# от нуля до n определенных запросов params
		def method(f):
			
			@wraps(f)
			def decorated_function2(*args, **kwargs):
				if len(args[1].GET.keys()) == 0:
					return f(*args, **kwargs)
				
				for i in set(args[1].GET.keys()):
					if i not in params:
						# если данные не совпадают
						cls.ERROR_URL_METHOD = """. Метод не ожидает параметров кроме
						{},""".format(params)
						return Response({
							'msg': cls.ERROR_URL_METHOD
						}, status=cls.status)
				return f(*args, **kwargs)
			return decorated_function2
		return method



from rest_framework.mixins import  CreateModelMixin
from django.db import IntegrityError
import re
from .tasks import send_email_recall


class RegistraionCreateApi(ApiView, CreateModelMixin):
	""" Принимаем запрос на регистрацию пользователей. 
	отправляем емейл если пришол запрос на созадние пользователя
	"""
	permission_classes = (AllowAny,)
	serializer_class = RecallClientSerializer
	queryset = RecallClient.objects.all()
		

	def post(self, request, *args, **kwargs):
		try:
			company = self.request.data.get('company')
			phone = self.request.data.get('phone')
			full_name = self.request.data.get('full_name')

			qqq, created = RecallClient.objects.get_or_create(
										company=company,
										phone=phone,
										full_name=full_name,
									)
			if created:
				self.RESPONSE_DEFAULT_MSG = self.SUCCESS_CREATE_MSG
				self.status =200

				send_email_recall.delay(company,full_name,phone)		
			else:
				self.RESPONSE_DEFAULT_MSG = self.RECALL_EXISTS_MSG
				self.status =200
		
		except IntegrityError as e:
			sub_re = re.findall(r"(?<=ОШИБКА:)[^\/]+", str(e))[0].strip()
			sub_detail = re.findall(r"(?<=DETAIL:)[^\/]+", str(e))[0].strip()

			self.RESPONSE_DEFAULT_MSG = '{} - {}'.format(sub_re, sub_detail)
			
		return Response({
				"msg": self.RESPONSE_DEFAULT_MSG
			},status=self.status)



class Logout(ApiView):
	""" Выходим из приложений:
	Удаляем auth_token, желательно удалять
	токен акторизации одновременно и из
	хранилища на клиенте"""
	permission_classes = (IsAuthenticated,)
	
	@CheckParams.check_get()
	def get(self, request, format=None):
		try:
			request.user.auth_token.delete()
			self.RESPONSE_DEFAULT_MSG = self.SUCCESS_GET_MSG
			self.status=200
		except:
			pass
		return Response({
				'msg': self.RESPONSE_DEFAULT_MSG
			}, status=self.status)


class CouponApiView(ApiView):
	""" Список все купонов
	Доступен всем и отображается не в
	без авторизации и регистрации
	"""
	permission_classes = (AllowAny,)
	
	get_url_params = {
		'category_id',
		}
	
	@CheckParams.check_any_get(params=get_url_params)
	def get(self, request):
		# собираем только активные купоны
		# принимает category_id и если оне есть,
		# выводим записи по категори, если нет, отдаем все
		try:
			coupon_list = Coupon.objects.filter(
				activate=True,
				discounter__category_id=request.GET['category_id']
				) if 'category_id' in request.GET else Coupon.objects.filter(activate=True)
			
			serializer = CouponSerializer(coupon_list, many=True)
			if serializer:
				self.status = 200
				self.RESPONSE_DEFAULT_MSG = self.SUCCESS_GET_MSG
		except:
			pass
		
		return Response({
			'data': serializer.data,
			'msg': self.RESPONSE_DEFAULT_MSG
			}, status=self.status)



class ClientRegistrationView(ApiView):
	""" Принимаем запрос на регистрацию пользователей. 
	Метод открыт для всех (AllowAny).
	Наследумеся от APIView rest_framework и, на всякий
	пожарный, если собираешься что-то менять прочти доку
	по рестфрймворку джанги """

	permission_classes = (AllowAny,)
	
	post_url_params = {
		'phone',
		'password'
		}
	
	@CheckParams.check(params=post_url_params)
	def post(self, request):
		# Передаем в класс сериализации полученные данны
		# телефон и пароль.
		# создаем объект сериализации модели пользователя
		# в ответ получаем объект пользователя
		# сохраняем объект в db и
		# возращаюем статус
		serialized = ClientRegistrationAccountSerializer(data=request.data)
		# пока не проверяем
		#if not ClientAccountSerializer.objects.filter(email=request.data.get('email')).exists():
		if serialized.is_valid():
			serialized.save()
			self.RESPONSE_DEFAULT_MSG = self.SUCCESS_CREATE_MSG
			self.status =200				
		else:
			self.RESPONSE_DEFAULT_MSG = self.ACCOUNT_EXISTS_MSG
		
		return Response({
				"msg": self.RESPONSE_DEFAULT_MSG
			},status=self.status)
		
class ResetPasswordView(ApiView):
	""" permission_classes = (AllowAny, )
	открываем доступ для всех
	и не забываем, что польщователь
	должен получить предупреждение о
	не распрастранении полученной в почте
	ссылки на страницу восстановления.
	
	Страница восстановления, это страница с
	уникальным идентификационным номером в ссылке,
	предоставляющая условно авторизированному
	пользователю, подтвердишему владение аккунтом(
				получив письмо со ссылко на страницу)
	производить перезапись личных авторизационных данных
	"""
	permission_classes = (AllowAny, )
	
	get_url_params = {
		'email'
		}

	@CheckParams.check_get(params=get_url_params)
	def get(self, request):
		# получаем запрос, берем email
		# ищем пользователя по полученному
		# почтовому ящику и отпавляем ему письмо
		try:
			key = random_string_generator(size=random.randint(30, 45)) 
			# ловим наличие пользователя
			try:
				user = Client.objects.get(email=request.GET['email'])
				
			except Client.DoesNotExist:
				user = Discounter.objects.get(email=request.GET['email'])
				
			user.reset_key = key
			user.save()
			# вызываем поток для отправки письма
			async_result = send_email_link.delay(request.GET['email'], user.first_name, user.reset_key)
			status = AsyncResult(str(async_result)).get()
			if status:
				self.RESPONSE_DEFAULT_MSG = 'сообщение отправлено на почту {}'.format(request.GET['email'])
				self.status=200
		
		except Discounter.DoesNotExist:
			# если пользователя с полученным 
			# email не найдено отправляем сообщение
			# об ошибке
			self.RESPONSE_DEFAULT_MSG = """что-то пошло не так, проверьте правильность
					почты: {} или обратитесь в поддержку""".format(request.GET['email'])
		
		return Response({
			'msg': self.RESPONSE_DEFAULT_MSG
			}, status=self.status)

class ClientFavoritesView(ApiView):
	""" Добавляем в избранное
	Извлекаем coupon_id из реквеста,
	и вызываем поток add_coupon_to_client_favorites
	отправляя id купона и клиента.
	"""
	permission_classes = (IsAuthenticated,)
	
	params = {
		'coupon_id',
		}
	
	@CheckParams.check_get()
	def get(self, request):
		import json
		
		try:
			# print(request.user)
			# print(type(request.user))
			# print(request.user.dynamic_username)
			# print(SkdkUser.objects.filter(dynamic_username = request.user.dynamic_username))

			# print(Client.objects.filter(dynamic_username = request.user.dynamic_username))

			[print(i) for i in Client._meta.get_fields()]
			asd = Client.objects.all()[0]
			print('*'*50)
			print(asd.skdkuser_ptr)
			print(asd.id)
			print(asd.dynamic_username)
			print(SkdkUser.objects.get(dynamic_username = request.user.dynamic_username).id)

			# client = Client.objects.get(skdkuser_ptr=SkdkUser.objects.filter(dynamic_username = request.user.dynamic_username))

			client = Client.objects.get(skdkuser_ptr=request.user)
			print(client)

			serializer = ClientFavoritesSerializer(client)
			self.data = serializer.data
			self.RESPONSE_DEFAULT_MSG = self.SUCCESS_GET_MSG
			self.status=200
		except:
			self.RESPONSE_DEFAULT_MSG = 'для доступа необходимо зарегистрировать аккаунт клиента'
		
		return Response({
			'data': self.data,
			'msg': self.RESPONSE_DEFAULT_MSG
			}, status=self.status)
		
	@CheckParams.check(params=params)
	def post(self, request):
		# принимает пост запрос с
		# параметром coupon_id
		# и добавляем купон в избарнное
		if request.method == 'POST':
			
			async_result = add_coupon_to_client_favorites.delay(
								request.POST['coupon_id'],
								request.user.id
								)
			
			status, msg = AsyncResult(str(async_result)).get()
			
			self.RESPONSE_DEFAULT_MSG = msg

			if status:
				self.status=200
				self.RESPONSE_DEFAULT_MSG = self.SUCCESS_CREATE_MSG

		return Response({
			'msg': self.RESPONSE_DEFAULT_MSG
			}, status=self.status)
	
	@CheckParams.check(params=params)
	def delete(self, request):
		# принимает DELETE запрос с
		# c параметро coupon_id
		# удаляем купон из избранного
		coupon_id = request.data['coupon_id']
		async_result = del_coupon_from_client_favorites.delay(
							coupon_id,
							request.user.id
							)
			
		res = AsyncResult(str(async_result))
		
		if res.get():
			self.status=200
			self.RESPONSE_DEFAULT_MSG = self.SUCCESS_DELETE_MSG
		
		return Response({
			'msg': self.RESPONSE_DEFAULT_MSG
			}, status=self.status)



class ClientSelfAccountView(ApiView):
	""" Аккаунт Клиента 
	Принимаем два типа запросов:
		get - отображаем данные личного кабинета,
		put - обновляем данные.

	При обновлении данных учитываем пустые поля.
	Считаем, что, если пользователь прислал пустое
	поле, занчит он сочел нужным оставить его пустым.
	"""
	parser_class = (FileUploadParser,)
	permission_classes = (IsAuthenticated, )
	
	put_url_params = {
		'first_name',
		'last_name',
		'email',
		'avatar',
		'age',
		'gender'
		}

	
	@CheckParams.check_get()
	def get(self, request):
		try:
			client = Client.objects.get(skdkuser_ptr=request.user)
			client.gender = client.GENDER_CHOICES[client.gender][1]
			serializer = ClientAccountSerializer(client)
			self.data = serializer.data
			
			self.RESPONSE_DEFAULT_MSG = self.SUCCESS_GET_MSG
			self.status=200
		except:
			self.RESPONSE_DEFAULT_MSG = 'для доступа необходимо зарегистрировать клиентский аккаунт'
		return Response({
			'data': self.data,
			'msg': self.RESPONSE_DEFAULT_MSG
		}, status=self.status)

	
	@CheckParams.check(params=put_url_params)
	def put(self, request):
		# обновляем данные клиента
		# принимаем авторизованного пользователя
		# в реквесте, берем его телефон.
		# принимает датасет
		# предварительно создаем в медиа директорию
		# c изображением (без записи в бд). После этого
		# меняем значение avatar в датасете на новое название
		# файла (см. generate_user_filename). 
		# передаем телефон и датасет в таск серели и в нём
		# записанным значением 
		# ImageField аватарки клиента будет
		# ссылка на эту директорию
		request.data._mutable = True
		
		client = Client.objects.get(skdkuser_ptr=request.user)
		
		request.data['avatar'] = default_storage.save(
					generate_user_filename(
							request.user,
							request.data['avatar']),
							request.data['avatar']
					) if len(request.FILES) else ''
		
		async_result = client_update.delay(
					client.id,
					request.data
					)
			
		res = AsyncResult(str(async_result))
		if res.get():
			self.RESPONSE_DEFAULT_MSG = self.SUCCESS_UPDATE_MSG
			self.status = 200
		
		return Response({
			'msg': self.RESPONSE_DEFAULT_MSG
			}, status=self.status)
	
	@CheckParams.check()
	def delete(self, request):
		try:
			request.user.auth_token.delete()
			Client.objects.get(
				skdkuser_ptr=request.user
				).delete()
			self.RESPONSE_DEFAULT_MSG = self.SUCCESS_DELETE_MSG
			self.status = 200
		except:
			self.RESPONSE_DEFAULT_MSG = self.ERROR_URL_METHOD

		return Response({
			'msg': self.RESPONSE_DEFAULT_MSG
			}, status=self.status)

class DiscounterRegistrationView(ApiView):
	""" Регистрируем дискоунтеров.
	При создании дискоуентар у него отсутсвует
	тариф и добавление купонов для него закрыто
	"""
	permission_classes = (AllowAny,)
	
	post_url_params = {
		'email',
		'password'
		}

	@CheckParams.check(params=post_url_params)
	def post(self, request):
		

		print(request.data)


		serialized = DiscounterRegistrationAccountSerializer(data=request.data)
		if serialized.is_valid(): 
			serialized.save()
			self.RESPONSE_DEFAULT_MSG = self.SUCCESS_UPDATE_MSG
			self.status = 200
		else:
			self.RESPONSE_DEFAULT_MSG = self.ACCOUNT_EXISTS_MSG
		
		return Response({
			'msg': self.RESPONSE_DEFAULT_MSG
		}, status=self.status)

class DiscounterAccountView(ApiView):
	""" Аккаунт Дискоунтера

	Принимаем два типа запросов:
		get - отображаем данные личного кабинета,
		put - обновляем данные.

	При обновлении данных учитываем пустые поля.
	Считаем, что, если пользователь прислал пустое
	поле, занчит он сочел нужным оставить его пустым.
	"""
	parser_class = (FileUploadParser,)
	permission_classes = (IsAuthenticated, )


	put_url_params = {
		'avatar',
		'cover_image',
		'company_name',
		'country',
		'city',
		'street',
		'hnum',
		'phone',
		'first_name',
		'last_name',
		'description',
		'category',
		'opening',
		'close'
		}
	
	@CheckParams.check_get()
	def get(self, request):
		try:
			discounter = Discounter.objects.get(skdkuser_ptr=request.user)
			serializer = DiscounterAccountSerializer(discounter)
			self.data = serializer.data
			self.RESPONSE_DEFAULT_MSG = self.SUCCESS_GET_MSG
			self.status = 200
		except:
			self.RESPONSE_DEFAULT_MSG = 'для доступа необходимо зарегистрировать аккаунт организации'
		
		
		return Response({
			'data': self.data,
			'msg': self.RESPONSE_DEFAULT_MSG
			}, status=self.status)
	
	@CheckParams.check(params=put_url_params)
	def put(self, request):
		# обновляем данные дискоунтера
		# принимаем авторизованного пользователя
		# в реквесте, берем его почту.
		# принимает датасет и отправляем это дело
		# в таск селери. Логика соответсвует апдейту клиента.
		request.data._mutable = True
		discounter = Discounter.objects.get(skdkuser_ptr=request.user)
		
		request.data['avatar'] = default_storage.save(
					generate_user_filename(
						request.user,
						request.data['avatar']),
						request.data['avatar']
					) if len(request.FILES) else discounter.avatar.name

		request.data['cover_image'] = default_storage.save(
					generate_user_filename(
						request.user,
						request.data['cover_image']),
						request.data['cover_image']
					) if len(request.FILES) else discounter.cover_image.name
				
		async_result = discounter_update.delay(
						discounter.id,
						request.data['category'],
						request.data
						)
			
		status, msg = AsyncResult(str(async_result)).get()
		
		self.RESPONSE_DEFAULT_MSG = msg
		
		if status:
			self.RESPONSE_DEFAULT_MSG = self.SUCCESS_UPDATE_MSG
			self.status = 200

		
		return Response({
			'msg': self.RESPONSE_DEFAULT_MSG
			}, status=self.status)

	@CheckParams.check()
	def delete(self, request):
		try:
			request.user.auth_token.delete()
			Discounter.objects.get(
				skdkuser_ptr=request.user
				).delete()
			self.RESPONSE_DEFAULT_MSG = self.SUCCESS_DELETE_MSG
			self.status = 200
		except:
			self.RESPONSE_DEFAULT_MSG = self.ERROR_URL_METHOD

		return Response({
			'msg': self.RESPONSE_DEFAULT_MSG
			}, status=self.status)


class CouponDiscounterView(ApiView):
	""" Добавляем купоны.
	методы:
		post - принимаем на запись,
		delete - принимает на удаление,
		put - принимаем на обновление,
			методы:
				del_image_by_id - принимает множество
				id изображений, который нужно удалить.
		get - вовзращаем весь список купонов
	
	производим запись в бд в отдельном потоке
	"""
	permission_classes = (IsAuthenticated, )
	
	post_params = {
		'files',
		'description',
		'validity',
		'percent',
		'limited_quantity',
		'repeatedly',
		}

	put_params = {
		'coupon_id',
		'files',
		'description',
		'validity',
		'repeatedly',
		'percent',
		'limited_quantity',
		'del_image_by_id'
		}
	
	del_params = {
		'coupon_id',
		}

	def update_or_create_coupon(self, method, data, user, coupon_id=None, files=False, delete_image_id=False):

		COMMON_STATUS = False
		
		filenames = ()

		if files:
			for file in files:
				filenames += (default_storage.save(
					generate_filename(
						user,
						data['description'],
						file), 
						file
						),)
		
		async_result = method.delay(
				data=data,
				coupon_id=coupon_id,
				user_id=user.id,
				del_img=delete_image_id,
				filenames=filenames
				)
		
		res = AsyncResult(str(async_result))

		COMMON_STATUS, exception_ = res.get()

		if COMMON_STATUS == False:
			return COMMON_STATUS, exception_
		
		return COMMON_STATUS, True
	
	@CheckParams.check_get()
	def get(self, request):
		# Отображаем список купонов
		# организации
		try:
			discounter = Discounter.objects.get(skdkuser_ptr=request.user)
			self.data = CouponDiscounterSerilizer(discounter).data
			self.RESPONSE_DEFAULT_MSG = self.SUCCESS_GET_MSG
			self.status = 200 
		except:
			pass	
		return Response({
			'data': self.data,
			'msg': self.RESPONSE_DEFAULT_MSG
			}, status=self.status)
	
	@CheckParams.check(params=post_params)
	def post(self, request):
		# Принимаем пост запрос на создание купона.
		# в параметрах обязательно должны быть данные
		# описание, срока действия и кол-ва раз использования
		# Купон может быть создан без изображний. files может
		# быть пустым.

		# для создания купона вызываем метод update_or_create_coupon
		# принимающий аргументов id юзера(дискоунтера), потоковый метод
		# из селери, словарь данных data и файлы.
		if request.method == "POST":
				
			add_status, exception_ = self.update_or_create_coupon(
						method=discounter_coupon_add,
						data={
							'description': request.data['description'],
							'validity': request.data['validity'],
							'repeatedly': request.data['repeatedly'],
							'percent': request.data['percent'],
							'limited_quantity': request.data['limited_quantity'],
							},
						user=request.user,
						files=request.FILES.getlist('files') if 'files' in request.FILES else False,
						)
				

			self.RESPONSE_DEFAULT_MSG = exception_
				
			if add_status != False:
				self.RESPONSE_DEFAULT_MSG = self.SUCCESS_CREATE_MSG
				self.status = 200
		

		return Response({
			'msg': self.RESPONSE_DEFAULT_MSG
			}, status=self.status)

	@CheckParams.check(params=put_params)
	def put(self, request):
		# редактируем купон
		# удаляя или добавляя изображения
		# PUT ..api/v1/account/discounter/?coupon_id=id&del_image_by_id
		# del_image_by_id необзательный параметр в этом методе
		add_status, exception_ = self.update_or_create_coupon(
						method=discounter_coupon_update,
						user=request.user,
						data={
							'description': request.data['description'],
							'validity': request.data['validity'],
							'repeatedly': request.data['repeatedly'],
							'percent': request.data['percent'],
							'limited_quantity': request.data['limited_quantity'],
							},
						coupon_id=request.data['coupon_id'],
						files=request.FILES.getlist('files') if 'files' in request.FILES else False,
						delete_image_id=[
							_id for _id in request.data['del_image_by_id'].split(',')
							] if 'del_image_by_id' in request.data else False,
						)

		self.RESPONSE_DEFAULT_MSG = exception_
			
		if add_status != False:
			self.RESPONSE_DEFAULT_MSG = self.SUCCESS_UPDATE_MSG
			self.status = 200
		
		return Response({
				'mag': self.RESPONSE_DEFAULT_MSG
			}, status=self.status)
	
	@CheckParams.check(params=del_params)
	def delete(self, request):
		# удаляем выбранное купоны
		# вместе со всем содержимым
		# DELETE ..api/v1/account/discounter/?coupon_id=id
		
		async_result = discounter_coupon_delete.delay(
								request.user.id,
								request.data['coupon_id']
								)
		res = AsyncResult(str(async_result))
		
		if res.get():
			self.RESPONSE_DEFAULT_MSG = self.SUCCESS_DELETE_MSG
			self.status = 200
		
		return Response({
			'msg': self.RESPONSE_DEFAULT_MSG
			}, status=self.status)


class AddDiscountBranchView(ApiView):
	""" Добавляем филиал организации 
	"""
	permission_classes = (IsAuthenticated, )
	
	post_params = {
		'avatar',
		'description',
		'address',
		'name'
		}

	put_params = {
		'branch_id',
		'avatar',
		'description',
		'address',
		'name',
		}

	del_params = {
		'branch_id',
		}
	
	@CheckParams.check_get()
	def get(self, request):
		if request.method == 'GET':
			branch = DiscountBranch.objects.select_related().filter(
						parent_shop_id=request.user.id
					)
			serializer = DiscounterBranchSerializer(branch, many=True)

			if serializer:
				self.RESPONSE_DEFAULT_MSG = self.SUCCESS_GET_MSG
				self.data = serializer.data
				self.status = 200
		
		return Response({
			'data': self.data,
			'msg': self.RESPONSE_DEFAULT_MSG
			}, status=self.status)
	
	@CheckParams.check(params=post_params)
	def post(self, request):
		if request.method == "POST":
				
			request.data._mutable = True
				
			request.data['avatar'] = default_storage.save(
						generate_user_filename(
							request.user,
							request.data['avatar']),
							request.data['avatar']
						) if len(request.FILES) else ''
		
			async_result = discounter_branch_add.delay(
					request.user.id,
					data={
						'avatar': request.data['avatar'],
						'description': request.data['description'],
						'address': request.data['address'],
						'name': request.data['name']
						})
				
			res = AsyncResult(str(async_result))
			
			if res.get():
				self.RESPONSE_DEFAULT_MSG = self.SUCCESS_CREATE_MSG
				self.status = 200 
		
		return Response({
			'msg': self.RESPONSE_DEFAULT_MSG
			}, status=self.status)
	
	@CheckParams.check(params=put_params)
	def put(self, request):
		
		request.data._mutable = True
		
		branch = DiscountBranch.objects.get(
					id=request.data['branch_id']
				)
		
		request.data['avatar'] = default_storage.save(
					generate_user_filename(
						request.user,
						request.data['avatar']),
						request.data['avatar']
					) if len(request.FILES) else branch.avatar.name
	
		async_result = discounter_branch_update.delay(
				request.data['branch_id'],
				data={
					'avatar': request.data['avatar'],
					'description': request.data['description'],
					'address': request.data['address'],
					'name': request.data['name'],
					})
			
		res = AsyncResult(str(async_result))	
		
		if res.get():
			self.RESPONSE_DEFAULT_MSG = self.SUCCESS_UPDATE_MSG
			self.status = 200
		
		return Response({
			'msg': self.RESPONSE_DEFAULT_MSG
			}, status=self.status)
	
	@CheckParams.check(params=del_params)
	def delete(self, request):
		async_result = discounter_branch_delete.delay(
				request.data['branch_id']
				)
		res = AsyncResult(str(async_result))	
		if res.get():
			self.RESPONSE_DEFAULT_MSG = self.SUCCESS_DELETE_MSG
			self.status = 200
		
		return Response({
			'msg': self.RESPONSE_DEFAULT_MSG
			}, status=self.status)

class DiscounterCategoryView(ApiView):
	""" Выводим список категорий """
	permission_classes = (AllowAny, )
	
	@CheckParams.check_get()
	def get(self, request):
		
		category = DiscounterCategory.objects.all()
		serializer = CeategorSerilezier(category, many=True)

		if serializer:
			self.RESPONSE_DEFAULT_MSG = self.SUCCESS_GET_MSG
			self.data = serializer.data
			self.status = 200
		
		return Response({
			'data': self.data,
			'msg': self.RESPONSE_DEFAULT_MSG
			}, status=self.status)

class DiscounterListView(ApiView):
	""" Общий список дискоутеров
	Если get_by_category_id отсутсвует,
	то отображаем весь список, иначе выводим
	список организаций в соотсветствии с id
	категорий в методе get_by_category_id
	"""
	
	permission_classes = (AllowAny, )
	
	get_url_params = {
		'get_by_category_id',
		'get_by_city_id'
		}
	
	def get_by_category_id(self, request):
		return Discounter.objects.filter(
			category_id__in=[_id for _id in request.GET['get_by_category_id'].split(',')],
			activate_rate__status=True
			) 

	def get_by_city_id(self, request):
		return Discounter.objects.filter(
			discounter_сity_id__in=[_id for _id in request.GET['get_by_city_id'].split(',')],
			activate_rate__status=True
			)

	def get_by_twice(self, request):
		return Discounter.objects.filter(
			category_id__in=[_id for _id in request.GET['get_by_category_id'].split(',')],
			discounter_сity_id__in=[_id for _id in request.GET['get_by_city_id'].split(',')],
			activate_rate__status=True
			)

	def get_all(self):
		return Discounter.objects.filter(
		 	activate_rate__status=True
		 	)
	
	@CheckParams.check_any_get(params=get_url_params)
	def get(self, request):
		if 'get_by_category_id' in request.GET and 'get_by_city_id' in request.GET:
			discounter_list = self.get_by_twice(request)

		elif 'get_by_category_id' in request.GET:
			discounter_list = self.get_by_category_id(request)


		elif 'get_by_city_id' in request.GET:
			discounter_list = self.get_by_city_id(request)
		
		else:
			discounter_list = self.get_all()
		
		serializer = DiscounterAllSerializer(discounter_list, many=True)

		if serializer:
			self.RESPONSE_DEFAULT_MSG = self.SUCCESS_GET_MSG
			self.data = serializer.data
			self.status = 200

		return Response({
			'data': self.data,
			'msg': self.RESPONSE_DEFAULT_MSG
			}, status=self.status)


class DiscounterOnClientAccountView(ApiView):
	""" отображаем организацию клиенту """
	
	permission_classes = (AllowAny, )
	
	get_url_params = {
		'id',
		}
	
	#@CheckParams.check_get(params=get_url_params)
	def get(self, request):
		try:
			discounter = Discounter.objects.get(id=request.GET['id'])
			serializer = DiscounterForClientSerializer(discounter)
			
			if serializer:
				self.RESPONSE_DEFAULT_MSG = self.SUCCESS_GET_MSG
			
			self.data = serializer.data
			self.status = 200
		
		except ValueError:
			self.RESPONSE_DEFAULT_MSG = 'id не может быть пустым'
		
		return Response({
			'data': self.data,
			'msg': self.RESPONSE_DEFAULT_MSG
			}, status=self.status)

class DiscounterCouponForClientList(ApiView):
	""" отображаем купоны организации клиенту """
	permission_classes = (AllowAny, )
	
	get_url_params = {
		'id',
		}
	
	@CheckParams.check_get(params=get_url_params)
	def get(self, request):
		try:
			coupon = Coupon.objects.filter(discounter__id=request.GET['id'])
			serializer = DiscounterForClientCouponSerializer(coupon, many=True)

			if serializer:
				self.RESPONSE_DEFAULT_MSG = self.SUCCESS_GET_MSG
			
			self.data = serializer.data
			self.status = 200
		
		except ValueError:
			self.RESPONSE_DEFAULT_MSG = 'id не может быть пустым'

		
		return Response({
			'data': self.data,
			'msg': self.RESPONSE_DEFAULT_MSG
			}, status=self.status)

# Аткивация купонов
# Если купон в избранном клиента,
# создаем к избранному купону объект
# активации определнного купона. Число
# активций объекта равно кол-ву раз активаций 
# в купоне. Если число активация превышает
# repeatedly то активация закончена.

class CouponUsedClient(ApiView):
	""" Отображаем использованные 
	клиентом купоны
	"""
	permission_classes = (IsAuthenticated, )
	
	data = None
	model = UsageCoupon
	
	post_url_params = {
		'coupon_id'
		}
	
	
	@CheckParams.check_get()
	def get(self, request):
		activations = self.model.objects.select_related().filter(
				client=request.user
			)
		
		serializer = ClientActivateSerializer(activations, many=True)
		
		if serializer:
			self.data = serializer.data
			self.RESPONSE_DEFAULT_MSG = self.SUCCESS_GET_MSG
		
		return Response({
				'data': self.data,
				'msg': self.RESPONSE_DEFAULT_MSG
			})

	
	@CheckParams.check(params=post_url_params)
	def post(self, request):
		async_result = coupon_activate.delay(
				request.data['coupon_id'],
				request.user.id
				)
		
		status, msg, code = AsyncResult(str(async_result)).get()
		
		if status:
			self.status = 200
		
		return Response({
				'code': code,
				'msg': msg
			}, status=self.status)

class DiscounterActivationView(ApiView):
	""" аткивация скидок для организаций
	отображаем новые активации и весь список
	активаций купонов организаций.
	Если клиент называет в организации правильный
	код (совпадающий с новой активацией, скидка проходит)
	"""
	permission_classes = (IsAuthenticated, )
	
	data = None
	
	@CheckParams.check()
	def get(self, request):
		
		activation_list = UsageCoupon.objects.select_related().filter(
						usage__discounter_id=request.user.id
					)

		serializer = ActivationForDiscounterSerializer(activation_list, many=True)
		
		if serializer:
			self.data = serializer.data
			self.RESPONSE_DEFAULT_MSG = self.SUCCESS_GET_MSG
			self.status = 200
		return Response({
			'data': self.data,
			'msg': self.RESPONSE_DEFAULT_MSG
			}, status=self.status) 

class Documentations(TemplateView):
	template_name = 'doc.html'

class ForntTestPageView(TemplateView):
	template_name = 'test.html'
